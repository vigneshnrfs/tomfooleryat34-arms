'use strict';

angular.module('core').controller('HeaderController', ['$scope', 'Authentication', 'Menus', '$mdSidenav', '$mdBottomSheet', '$location',
    function ($scope, Authentication, Menus, $mdSidenav, $mdBottomSheet, $location) {
        $scope.authentication = Authentication;
        $scope.isCollapsed = false;
        $scope.menu = Menus.getMenu('topbar');

        $scope.toggleCollapsibleMenu = function () {
            $scope.isCollapsed = !$scope.isCollapsed;
        };

        $scope.toggleMenu = function () {
            $mdSidenav('left').toggle();
        };

        $scope.showBottomSheet = function ($event, menu) {
            $mdBottomSheet.show({
                templateUrl: 'modules/core/views/bottomsheet.client.view.tpl.html',
                controller: 'SubMenuController',
                targetEvent: $event,

                locals: menu,
                controllerAs: 'ctrl'
            }).then(function (menu) {
                console.log(menu);
                $location.url(menu.link);
            });
        };

        // Collapsing the menu after navigation
        $scope.$on('$stateChangeSuccess', function () {
            $scope.isCollapsed = false;
        });
    }
]);


angular.module('core').controller('SubMenuController', ['$scope', 'Authentication', 'Menus', '$mdSidenav', '$mdBottomSheet', 'locals',
    function ($scope, Authentication, Menus, $mdSidenav, $mdBottomSheet, locals) {
        $scope.authentication = Authentication;
        $scope.isCollapsed = false;
        $scope.menus = locals;
        console.log($scope);
        console.log(locals);
        $scope.listItemClick = function (menu) {
            $mdBottomSheet.hide(menu);
        };
    }
]);
