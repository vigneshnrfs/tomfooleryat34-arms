'use strict';

angular.module('core').factory('ToastService', ['$mdToast',
	function($mdToast) {
		// Toastservice service logic
		// ...
		var toastPosition = {
			bottom:false,
			top: true,
			left: false,
			right: true
		};
		var getToastPosition = function(){
			return Object.keys(toastPosition).filter(function(pos){
				return toastPosition[pos];
			}).join(' ');
		};

		// Public API
		return {
			openToast: function(content) {
				$mdToast.show(
					$mdToast.simple()
						.content(content)
						.position(getToastPosition())
						.hideDelay(3000)
				);
				return true;
			}
		};
	}
]);
