'use strict';

//Menuitems service used to communicate Menuitems REST endpoints
angular.module('menuitems').factory('MenuItems', ['$resource',
	function($resource) {
		return $resource('menuitems/:menuItemId', { menuItemId: '@_id'
		}, {
			update: {
				method: 'PUT'
			}

		});
	}
]);
