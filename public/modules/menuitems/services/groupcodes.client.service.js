'use strict';

//GroupCodes service used to communicate GroupCodes REST endpoints
angular.module('menuitems').factory('GroupCodes', ['$resource',
    function($resource) {
        return $resource('pos/groupcode/:groupCodeId', { groupCodeId: '@_id'
        }, {
            update: {
                method: 'PUT'
            }

        });
    }
]);
