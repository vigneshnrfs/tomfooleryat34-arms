'use strict';

//Setting up route
angular.module('menuitems').config(['$stateProvider',
    function ($stateProvider) {
        // Menuitems state routing
        $stateProvider.
            state('listMenuItems', {
                url: '/menuitems',
                templateUrl: 'modules/menuitems/views/list-menuitems.client.view.html'
            }).
            state('createMenuItem', {
                url: '/menuitems/create',
                templateUrl: 'modules/menuitems/views/create-menuitem.client.view.html'
            }).
            state('viewMenuItem', {
                url: '/menuitems/:menuItemId',
                templateUrl: 'modules/menuitems/views/view-menuitem.client.view.html'
            }).
            state('editMenuItem', {
                url: '/menuitems/:menuItemId/edit',
                templateUrl: 'modules/menuitems/views/edit-menuitem.client.view.html'
            }).
            state('createGroupCode', {
                url: '/groupcode/create',
                templateUrl: 'modules/menuitems/views/create-groupcode.client.view.html'
            });
    }
]);
