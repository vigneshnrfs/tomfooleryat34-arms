'use strict';

// Menuitems controller
angular.module('menuitems').controller('MenuItemsController', ['$scope', '$filter', '$stateParams', '$location',
    '$mdDialog', 'Authentication', 'MenuItems', 'Recipes','lodash',
    function ($scope, $filter, $stateParams, $location, $mdDialog, Authentication, Menuitems, Recipes,lodash) {
        $scope.authentication = Authentication;

        $scope.recipes = null;

        $scope.createInit = function () {
            $scope.menuItem = {portions: [{name: 'Normal'}]};
            $scope.recipes = Recipes.query();
        };



        $scope.recipeChanged = function (portion) {

            for (var i in $scope.recipes) {
                if (portion.activeRecipe === $scope.recipes[i]._id) {
                    portion.cost = $scope.recipes[i].portionCost;
                    break;
                }
            }
            $scope.updateGP(portion);
        };

        $scope.removePortion = function (idx) {
            $scope.menuItem.portions.splice(idx, 1);

        };

        $scope.updateGP = function (portion) {
            if (portion.cost && portion.price) {
                portion.grossProfit = (portion.price - portion.cost) * 100 / portion.price;
            }
        };

        // Create new MenuItem
        $scope.create = function () {
            // Create new MenuItem object
            var menuitem = new Menuitems($scope.menuItem);

            // Redirect after save
            menuitem.$save(function (response) {
                $location.path('menuitems/' + response._id);

                // Clear form fields
                $scope.name = '';
            }, function (errorResponse) {
                $scope.error = errorResponse.data.message;
                console.error($scope.error);
            });
        };

        // Remove existing MenuItem
        $scope.remove = function (menuitem) {
            if (menuitem) {
                menuitem.$remove();

                for (var i in $scope.menuitems) {
                    if ($scope.menuitems [i] === menuitem) {
                        $scope.menuitems.splice(i, 1);
                    }
                }
            } else {
                $scope.menuitem.$remove(function () {
                    $location.path('menuitems');
                });
            }
        };

        // Update existing MenuItem
        $scope.update = function () {
            var menuitem = $scope.menuItem;

            menuitem.$update(function () {
                $location.path('menuitems/' + menuitem._id);
            }, function (errorResponse) {
                $scope.error = errorResponse.data.message;
            });
        };

        // Find a list of Menuitems
        $scope.find = function () {
            var menuitems = Menuitems.query();
            menuitems.$promise.then(function(result){
                $scope.menuItems = lodash.groupBy(result,'groupCode');
                console.log($scope.menuItems);
            });


        };

        // Find existing MenuItem
        $scope.findOne = function () {
            $scope.menuItem = Menuitems.get({
                menuItemId: $stateParams.menuItemId
            });

        };

        //Initialisation function for Edit View
        $scope.editInit = function () {

            $scope.menuItem = Menuitems.get({
                menuItemId: $stateParams.menuItemId
            });
            $scope.recipes = Recipes.query();


            $scope.menuItem.$promise.then(function (data) {
                console.log(data);
                for (var i in data.portions) {
                    data.portions[i].activeRecipe = data.portions[i].activeRecipe._id;
                }
            });
        };

        /*
         $scope.updateGroupCode = function(ev,id){
         var data = null;
         if(id){
         for(var i in $scope.groupCodesMaster){
         if ($scope.groupCodesMaster[i]._id === id){
         data = $scope.groupCodesMaster[i];
         break;
         }
         }
         $scope.showGroupCodeDialog(ev,data);
         } else {
         $scope.showGroupCodeDialog(ev,null);
         }
         };

         $scope.showGroupCodeDialog = function (ev, data) {


         $mdDialog.show({
         controller: 'GroupCodesController',
         controllerAs:'ctrl',
         templateUrl: 'modules/menuitems/views/create-groupcode.client.view.html',
         parent: angular.element(document.body),
         targetEvent: ev,

         bindToController: true,
         locals: {
         groupCode: data
         },

         })
         .then(function (groupCode) {

         console.log('Dialog Closed.');
         $scope.groupCodesMaster = GroupCodes.query();
         }, function () {
         console.log('Dialog Cancelled');
         });
         }
         ;
         */

    }
])
;
