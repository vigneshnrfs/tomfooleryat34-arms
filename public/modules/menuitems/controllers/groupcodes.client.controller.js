'use strict';

angular.module('menuitems').controller('GroupCodesController', ['$scope','$mdDialog','GroupCodes','ToastService',
	function($scope,$mdDialog,GroupCodes,ToastService) {
		// Groupcode controller logic
		// ...

		$scope.init = function(){
			console.log('Calling createInit()');
			console.log('data = ',$scope.data);
			console.log($scope);

		};

		// Create new GroupCode
		$scope.create = function (data) {
			//console.log('Calling Create GroupCode()',groupCode);
			// Create new GroupCode object
			var groupCode = new GroupCodes(data);

			// Redirect after save
			groupCode.$save(function (response) {
				ToastService.openToast('GroupCode ' + response.name + ' created!');
				$mdDialog.hide();



			}, function (errorResponse) {
				$scope.error = errorResponse.data.message;
				console.error($scope.error);
			});
		};

		$scope.update = function(groupCode){
			console.log('Calling Update()');
			groupCode.$update(function (response) {
				ToastService.openToast('GroupCode ' + response.name + ' updated!');
				$mdDialog.hide();
			}, function (errorResponse) {
				$scope.error = errorResponse.data.message;
				console.error($scope.error);
			});
		};
	}
]);
