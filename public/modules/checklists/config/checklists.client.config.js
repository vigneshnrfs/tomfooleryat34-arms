'use strict';

// Configuring the Articles module
angular.module('checklists').run(['Menus',
	function(Menus) {
		// Set top bar menu items
		Menus.addMenuItem('topbar', 'Checklists', 'checklists', 'dropdown', '');
		Menus.addSubMenuItem('topbar', 'checklists', 'Edit Day Templates', 'checklists/template/tabs/day');
		Menus.addSubMenuItem('topbar', 'checklists', 'Edit Night Templates', 'checklists/template/tabs/night');
		Menus.addSubMenuItem('topbar', 'checklists', '----', '');
		Menus.addSubMenuItem('topbar', 'checklists', 'Day Opening Checklist', 'checklists/opening/day');
		Menus.addSubMenuItem('topbar', 'checklists', 'Day Closing Checklist', 'checklists/closing/day');
		Menus.addSubMenuItem('topbar', 'checklists', '----', '');
		Menus.addSubMenuItem('topbar', 'checklists', 'Night Opening Checklist', 'checklists/opening/night');

		Menus.addSubMenuItem('topbar', 'checklists', 'Night Closing Checklist', 'checklists/closing/night');
	}
]);
