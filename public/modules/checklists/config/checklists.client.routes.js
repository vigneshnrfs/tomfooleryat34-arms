'use strict';

//Setting up route
angular.module('checklists').config(['$stateProvider',
    function ($stateProvider) {
        // Checklists state routing
        $stateProvider.
            state('addTaskToOpeningTemplate', {
                url: '/checklists/template/opening/:shift',
                templateUrl: 'modules/checklists/views/checklist.opening.template.client.view.html'
            }).
            state('addTaskToClosingTemplate', {
                url: '/checklists/template/closing/:shift',
                templateUrl: 'modules/checklists/views/checklist.closing.template.client.view.html'
            }).
            state('templatesView', {
                url: '/checklists/template/tabs/:shift',
                templateUrl: 'modules/checklists/views/tab-checklist.template.client.view.html'
            }).
            state('checklistOpening', {
                url: '/checklists/opening/:shift?date',
                templateUrl: 'modules/checklists/views/checklist.opening.client.view.html'
            }).
            state('checklistClosing', {
                url: '/checklists/closing/:shift?date',
                templateUrl: 'modules/checklists/views/checklist.closing.client.view.html'
            });
    }
]);













