'use strict';

//Checklists service used to communicate Checklists REST endpoints
angular.module('checklists').factory('Checklists', ['$resource',
	function($resource) {
		return $resource('checklists/:checklistId', { checklistId: '@_id'
		}, {
			update: {
				method: 'PUT'
			}
		});
	}
]);


//Checklists service used to communicate Opening Checklist Template REST endpoints
angular.module('checklists').factory('ChecklistsOpeningTemplate', ['$resource',
	function($resource) {
		return $resource('checklists/template/opening/:openingChecklistTaskId', { openingChecklistTaskId: '@_id'
		}, {
			update: {
				method: 'PUT'
			}
		});
	}
]);


//Checklists service used to communicate Closing Checklist Template REST endpoints
angular.module('checklists').factory('ChecklistsClosingTemplate', ['$resource',
	function($resource) {
		return $resource('checklists/template/closing/:closingChecklistTaskId', { closingChecklistTaskId: '@_id'
		}, {
			update: {
				method: 'PUT'
			}
		});
	}
]);

//Checklists service used to communicate Opening Checklist REST endpoints
angular.module('checklists').factory('ChecklistsOpening', ['$resource',
	function($resource) {
		return $resource('checklists/opening/:openingChecklistId', { openingChecklistId: '@_id'
		}, {
			update: {
				method: 'PUT'
			},
			updateTask:{
				method:'PATCH'
			}
		});
	}
]);


//Checklists service used to communicate Opening Checklist REST endpoints
angular.module('checklists').factory('ChecklistsClosing', ['$resource',
	function($resource) {
		return $resource('checklists/closing/:closingChecklistId', { closingChecklistId: '@_id'
		}, {
			update: {
				method: 'PUT'
			},
			updateTask:{
				method:'PATCH'
			}
		});
	}
]);
