'use strict';

// Checklists controller
angular.module('checklists').controller('ChecklistsClosingTemplateController', ['$scope', '$stateParams', '$location',
	'Authentication', 'ChecklistsClosingTemplate','lodash','$mdDialog',
	function($scope, $stateParams, $location, Authentication, ChecklistsClosingTemplate,
			 lodash,$mdDialog) {
		$scope.authentication = Authentication;
		$scope.shift = $stateParams.shift;
		$scope.task ={};
		$scope.task.shift = $stateParams.shift;


		// Create new Checklist
		$scope.create = function() {
			// Create new Checklist object
			var checklistTask = new ChecklistsClosingTemplate ($scope.task);
			checklistTask.shift = $stateParams.shift;
			//console.log($scope.task);
			// Redirect after save
			checklistTask.$save(function(response) {

				$scope.find();
				// Clear form fields
				$scope.task = {group:'',taskName:''};
				console.log($scope.task_form);
				$scope.task_form.$pristine = true;
				$scope.task_form.$dirty = false;
			}, function(errorResponse) {
				console.log('----\nError Occurred when saving the Task Template');
				console.error(errorResponse.data.message);
				console.log('----');
				$scope.error = errorResponse.data.message;

			});
		};

		// Remove existing Checklist
		$scope.remove = function(checklist) {
			if ( checklist ) { 
				checklist.$remove(function(){
					$scope.find();
				});
			}
		};

		// Update existing Checklist
		$scope.update = function() {
			var checklist = $scope.task;

			checklist.$update(function() {
				$scope.find();
			}, function(errorResponse) {
				console.log('----\nError Occurred when saving the Task Template');
				console.error(errorResponse.data.message);
				console.log('----');
				$scope.error = errorResponse.data.message;
			});
		};

		// Find a list of Checklists
		$scope.find = function() {
			ChecklistsClosingTemplate.query({'shift': $stateParams.shift}).$promise.then(function(data){
				$scope.tasks = lodash.groupBy(data,'group');
				console.log($scope.tasks);
			});

		};

		// Find existing Checklist
		/*
		$scope.findOne = function() {
			$scope.checklist = Checklists.get({ 
				checklistId: $stateParams.checklistId
			});
		};

		*/

		//On Edit Button Clicked pass the data to the form.
		$scope.editTask = function(task){
			$scope.task = task;
		};

		//Confirm delete dialog
		$scope.showDeleteConfirm = function(ev,task){
			var confirm = $mdDialog.confirm()
				.title('Are you sure?')
				.content('Deleting the task from the template means the tasks will not be visible in the subsequent checklists.')
				.ariaLabel('Are you sure to delete?')
				.ok('I understand!')
				.cancel('Cancel')
				.targetEvent(ev);

			$mdDialog.show(confirm).then(function(){
				//Confirm so call delete function.
				$scope.remove(task);

			});
		};
	}
]);
