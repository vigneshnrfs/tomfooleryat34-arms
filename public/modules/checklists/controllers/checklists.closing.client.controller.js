'use strict';

// Checklists controller
angular.module('checklists').controller('ChecklistsClosingController', ['$scope', '$stateParams', '$location',
	'Authentication', 'ChecklistsClosing','lodash','$mdDialog',
	function($scope, $stateParams, $location, Authentication, ChecklistsClosing,lodash,$mdDialog) {
		$scope.authentication = Authentication;

		//Get date info from the url
		if($stateParams.date){
			$scope.checklistDate = new Date($stateParams.date);
		} else {
			$scope.checklistDate = new Date();
		}


		// Create new Checklist
		$scope.create = function() {
			// Create new Checklist object
			/*
			var checklist = new Checklists ({
				name: this.name
			});

			// Redirect after save
			checklist.$save(function(response) {
				$location.path('checklists/' + response._id);

				// Clear form fields
				$scope.name = '';
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});

			*/
		};

		// Remove existing Checklist
		$scope.remove = function(checklist) {
			if ( checklist ) { 
				checklist.$remove();

				for (var i in $scope.checklists) {
					if ($scope.checklists [i] === checklist) {
						$scope.checklists.splice(i, 1);
					}
				}
			} else {
				$scope.checklist.$remove(function() {
					$location.path('checklists');
				});
			}
		};

		// Update existing Checklist
		$scope.update = function(task) {
			$scope.isDataLoading = true;
			var checklist = $scope.checklistMeta;
			for(var i in checklist.checklists ){
				if(checklist.checklists[i].user){
					checklist.checklists[i].user = checklist.checklists[i].user._id;
				}
			}

			checklist.$update(function() {
					console.info('Task Updated');
				$scope.find();
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
				console.log($scope.error);
				$scope.isDataLoading = false;
			});
		};

		// Find a list of Checklists
		$scope.find = function(date) {
			$scope.isDataLoading = true;
			if(!date) {
				date = $scope.checklistDate;
			}

			if($stateParams.shift === 'day'){
				date.setHours(8,0,0,0);
			} else if($stateParams.shift === 'night') {
				date.setHours(17, 0, 0, 0);
			}
				ChecklistsClosing.query({date: date,shift: $stateParams.shift}).$promise.then(function(data){
				console.log(data);
				if(data.length === 0){
					$scope.checklistMeta = null;
					$scope.checklists = null;
					$scope.isDataLoading = false;
				} else {
					$scope.checklistMeta = data[0];
					$scope.checklists = lodash.groupBy(data[0].checklists,'group');
					console.log($scope.checklists);
					$scope.isDataLoading = false;
				}

			});
		};

		// Find existing Checklist
		$scope.findOne = function() {
			/*
			$scope.checklist = Checklists.get({
				checklistId: $stateParams.checklistId
			});
			*/
		};

		$scope.addComment = function(task){
			alert(' test');
		};

		//Update a single task in the checklist
		$scope.updateTask = function(task){
			$scope.isDataLoading = true;
			console.info('Update task called...');
			task.taskId = task._id;
			if(task.user){delete task.user;}
			task._id = $scope.checklistMeta._id;
			ChecklistsClosing.updateTask(task).$promise.then(function(data){
				console.log('Task Update Successful.');
				$scope.find();
			},function(err){
				console.error(err);
				$scope.isDataLoading = false;
			});
		};

		$scope.closeChecklist = function(){
			$scope.checklistMeta.isLocked = true;
			$scope.update();
		};

		$scope.unlockChecklist = function(){
			$scope.checklistMeta.isLocked = false;
			$scope.update();
		};

	}
]);
