'use strict';

// Checklists controller
angular.module('checklists').controller('ChecklistsTabController', ['$scope', '$stateParams', '$location', 'Authentication', 'Checklists',
	function($scope, $stateParams, $location, Authentication, Checklists) {
	 if($stateParams.shift === 'day'){
		 $scope.tablabel = 'Day Shift Supervisor Checklists';
	 } else if($stateParams.shift === 'night'){
		 $scope.tablabel = 'Evening/Night Shift Supervisor Checklists';
	 }
	}
]);
