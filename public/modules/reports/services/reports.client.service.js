'use strict';

//Reports service used to communicate Reports REST endpoints
angular.module('reports').factory('Reports', ['$http',
	function($http) {
		var service = {};

		service.sales = function(dateFilters){
			return $http({url: 'reports/sales',
			method: 'GET',
			params: dateFilters});
		};

		return service;
	}
]);
