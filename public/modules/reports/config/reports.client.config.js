'use strict';

// Configuring the Articles module
angular.module('reports').run(['Menus',
	function(Menus) {
		// Set top bar menu items
		Menus.addMenuItem('topbar', 'Reports', 'reports', 'dropdown', '/reports(/create)?');
		Menus.addSubMenuItem('topbar', 'reports', 'Sales Report', 'reports/sales');
		Menus.addSubMenuItem('topbar', 'reports', 'New Report', 'reports/create');
	}
]);
