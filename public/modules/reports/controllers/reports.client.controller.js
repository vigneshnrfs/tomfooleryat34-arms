'use strict';

// Reports controller
angular.module('reports').controller('ReportsController', ['$scope', '$stateParams', '$location', 'Authentication', 'Reports', 'lodash',
    function ($scope, $stateParams, $location, Authentication, Reports, lodash) {
        $scope.authentication = Authentication;
        $scope.salesReport = null;

        $scope.salesInit = function () {
            var fromDate = new Date();
            var toDate = new Date();
            console.log('Sales Report Initialising...');
            $scope.getSalesReport({fromDate: fromDate, toDate: toDate});
        };

        $scope.getSalesReport = function (filters) {
            $scope.isDataLoading = true;
            filters.fromDate.setHours(6, 0, 0, 0);
            filters.toDate.setDate(filters.toDate.getDate() + 1);
            filters.toDate.setHours(4, 0, 0, 0);
            //console.log(filters);

            Reports.sales(filters).success(function (data) {
                console.log(data);
                $scope.salesItems = lodash.groupBy(data.salesDetail, 'GroupCode');

                var s = lodash.reduce(data, function (s, entry) {
                    return s + parseFloat(entry.Price) * parseFloat(entry.Quantity);
                }, 0);

                var totalQty = lodash.reduce(data.salesDetail, function (totalQty, entry) {
                    return totalQty + parseFloat(entry.Quantity);
                }, 0);

                $scope.meta = data.floorCountAndTotals;
                var meta = $scope.meta;
                $scope.meta.total = {};
                $scope.meta.total.covers = meta.firstFloor.covers + meta.groundFloor.covers + meta.barTab.covers;
                $scope.meta.total.sales = meta.firstFloor.sales + meta.groundFloor.sales + meta.barTab.sales;

                $scope.totalQty = totalQty;
                $scope.isDataLoading = false;

            }).error(function (error) {
                console.error(error);
            });
        };


        // Create new Report
        $scope.create = function () {
            // Create new Report object
            var report = new Reports({
                name: this.name
            });

            // Redirect after save
            report.$save(function (response) {
                $location.path('reports/' + response._id);

                // Clear form fields
                $scope.name = '';
            }, function (errorResponse) {
                $scope.error = errorResponse.data.message;
            });
        };

        // Remove existing Report
        $scope.remove = function (report) {
            if (report) {
                report.$remove();

                for (var i in $scope.reports) {
                    if ($scope.reports [i] === report) {
                        $scope.reports.splice(i, 1);
                    }
                }
            } else {
                $scope.report.$remove(function () {
                    $location.path('reports');
                });
            }
        };

        // Update existing Report
        $scope.update = function () {
            var report = $scope.report;

            report.$update(function () {
                $location.path('reports/' + report._id);
            }, function (errorResponse) {
                $scope.error = errorResponse.data.message;
            });
        };

        // Find a list of Reports
        $scope.find = function () {
            $scope.reports = Reports.query();
        };

        // Find existing Report
        $scope.findOne = function () {
            $scope.report = Reports.get({
                reportId: $stateParams.reportId
            });
        };
    }
]);
