'use strict';

// Suppliers controller
angular.module('suppliers').controller('SuppliersController', ['$scope', '$stateParams', '$location','$mdDialog','Authentication','ToastService', 'Suppliers','Products',
	function($scope, $stateParams, $location,$mdDialog, Authentication,ToastService, Suppliers,Products) {
		$scope.authentication = Authentication;

		// Create new Supplier
		$scope.create = function() {
			// Create new Supplier object
			var supplier = new Suppliers ({
				name: this.name
			});

			// Redirect after save
			supplier.$save(function(response) {
				$location.path('suppliers/' + response._id);

				// Clear form fields
				$scope.name = '';
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		// Remove existing Supplier
		$scope.remove = function(supplier) {
			if ( supplier ) { 
				supplier.$remove();

				for (var i in $scope.suppliers) {
					if ($scope.suppliers [i] === supplier) {
						$scope.suppliers.splice(i, 1);
					}
				}
			} else {
				$scope.supplier.$remove(function() {
					$location.path('suppliers');
				});
			}
		};

		// Update existing Supplier
		$scope.update = function() {
			var supplier = $scope.supplier;

			supplier.$update(function() {
				$location.path('suppliers/' + supplier._id);
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		// Find a list of Suppliers
		$scope.find = function() {
			$scope.suppliers = Suppliers.query();
		};

		// Find existing Supplier
		$scope.findOne = function() {
			$scope.supplier = Suppliers.get({ 
				supplierId: $stateParams.supplierId
			});

		};


		//Remove a product
		$scope.removeProduct = function(data){
			var product = new Products({supplierId:$stateParams.supplierId, _id: data._id});
			product.$remove(function(){

				for (var i in $scope.supplier.products) {
					if ($scope.supplier.products [i]._id === product._id) {
						$scope.supplier.products.splice(i, 1);
					}
				}

				ToastService.openToast('Product removed from system');

			});
		};

		//Update the products when a new product is added.
		$scope.$on('supplierProductAdded',function(event,data){
			ToastService.openToast('Product Added');
			$scope.supplier.products.push(data);
		});

		//Broadcast edit product to product controller
		$scope.editProduct = function(product){
			$scope.$broadcast('editSupplierProduct',product);
		};
	}
]);
