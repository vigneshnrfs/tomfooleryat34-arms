'use strict';

angular.module('suppliers').controller('ProductsController', ['$scope','$stateParams','Products','ToastService',
	function($scope,$stateParams,Products,ToastService) {
		// Products controller logic
		// ...
		// Create new Supplier
		var supplierId = $stateParams.supplierId;
		$scope.product = {};

		$scope.$on('editSupplierProduct',function(event,product){
			console.dir(product);
			$scope.product = product;
		});
		$scope.editProduct = function(product){
			$scope.product = product;
		};

		//Create or Update Product
		$scope.createOrUpdate = function() {

			// Create new Supplier object
			var productData = $scope.product;
			productData.supplierId = supplierId; //required for ngresource
			var product = new Products(productData);

			if($scope.product._id){
				//Update the  product
				product.$update(function(response){

					ToastService.openToast('Product Updated');
					$scope.product = {};
				}, function(errorresponse){
					console.error(errorresponse.data);
					ToastService.openToast('Error Occurred when updating the product.');
				});
			} else {
				//Create a new product to the supplier
				product.$save(function(response) {
					console.log(response);
					$scope.product = {};
					$scope.$emit('supplierProductAdded',response);
				}, function(errorResponse) {
					console.error(errorResponse.data.message);
				});
			}
		};


	}
]);
