'use strict';


angular.module('suppliers').factory('Products', ['$resource',
	function($resource) {
		return $resource('suppliers/:supplierId/products/:productId', { supplierId: '@supplierId', productId:'@_id'
		}, {
			update: {
				method: 'PUT'
			},
			getAll:{
				method:'GET',
				isArray:true,
				url: 'products'
			}

		});
	}
]);
