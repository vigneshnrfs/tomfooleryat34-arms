'use strict';

// Recipes controller
angular.module('recipes').controller('RecipesController', ['$scope', '$stateParams', '$location', 'Authentication', 'Recipes','Products',
	function($scope, $stateParams, $location, Authentication, Recipes,Products) {
		$scope.authentication = Authentication;

		//Initialisation Function
		$scope.init = function(){
			$scope.recipe = {
				name:'',
				weight:0,
				yield:0,
				totalWeight:0,
				portionSize:1,
				totalCost:0,
				portionCost:0,
				ingredientItems: [],
				baseRecipeItems:[]
			};

			//Get all your Ingredients
			$scope.ingredientMaster = Products.getAll();
			console.debug('Ingredients List',$scope.ingredientMaster);

			$scope.baseRecipeMaster = Recipes.query();
			console.debug($scope.baseRecipeMaster);
		};

		//Add a new ingredient row to the form
		$scope.addIngredientRow = function(){
			$scope.recipe.ingredientItems.push({});
		};

		//Remove a new ingredient row to the form
		$scope.removeIngredientRow = function(idx){
			$scope.recipe.ingredientItems.splice(idx,1);
			$scope.updateIngredientFields();
		};

		//Add a new baseRecipe row to the form
		$scope.addBaseRecipeRow = function(){
			$scope.recipe.baseRecipeItems.push({});
		};

		//Remove the baseRecipe row to the form
		$scope.removeBaseRecipeRow = function(idx){
			$scope.recipe.baseRecipeItems.splice(idx,1);
			$scope.updateBaseRecipeFields();
		};

		//Update all the calculated fields for the ingredient section
		$scope.updateIngredientFields = function(){
			//console.info('Calling updateIngredientFields...');
			var recipe = $scope.recipe;
			var ingredientMaster = $scope.ingredientMaster;
			//console.log(recipe.ingredientItems);
			//console.log(ingredientMaster);
			//Match the ingredient from the ingredient master
			for(var i in recipe.ingredientItems){
				for(var j in ingredientMaster){
					if(recipe.ingredientItems[i].ingredient === ingredientMaster[j]._id){

						//Update Ingredient Unit Cost
						recipe.ingredientItems[i].unitCost = ingredientMaster[j].unitPrice;
						//Update units
						recipe.ingredientItems[i].units = ingredientMaster[j].units;
						//Update Ingredient Portion Cost
						recipe.ingredientItems[i].totalCost = recipe.ingredientItems[i].unitCost * recipe.ingredientItems[i].quantity;
					}
				}
			}

			$scope.updateCalculatedFields();

		};

		$scope.updateBaseRecipeFields = function(){
			console.info('Calling updateBaseRecipeFields...');
			var recipe = $scope.recipe;
			var recipeMaster = $scope.baseRecipeMaster;
			//Match the ingredient from the ingredient master
			for(var i in recipe.baseRecipeItems){
				for(var j in recipeMaster){
					if(recipe.baseRecipeItems[i].recipe === recipeMaster[j]._id){
						//Update BaseRecipe Unit Cost
						recipe.baseRecipeItems[i].unitCost = recipeMaster[j].unitCost;
						//Update Ingredient Portion Cost
						recipe.baseRecipeItems[i].totalCost = recipe.baseRecipeItems[i].unitCost * recipe.baseRecipeItems[i].quantity;
					}
				}
			}

			$scope.updateCalculatedFields();
		};

		$scope.updateCalculatedFields = function(){
			var recipe = $scope.recipe;
			recipe.weight = 0;
			recipe.totalCost = 0;

			console.debug('Calling updateCalculatedFields()');

			for(var i in recipe.ingredientItems){
				if(recipe.ingredientItems[i].quantity){
					recipe.weight = recipe.weight + recipe.ingredientItems[i].quantity;
					recipe.totalCost = recipe.totalCost + recipe.ingredientItems[i].totalCost;
				}
			}

			for(var j in recipe.baseRecipeItems){
				if(recipe.baseRecipeItems[j].quantity){
					recipe.weight = recipe.weight + recipe.baseRecipeItems[j].quantity;
					recipe.totalCost = recipe.totalCost + recipe.baseRecipeItems[j].totalCost;
				}

			}


			recipe.totalWeight = recipe.yield + recipe.weight;
			recipe.portionCost = recipe.totalCost / recipe.portionSize;
			recipe.unitCost = recipe.totalCost / recipe.totalWeight;
		};

		// Create new Recipe
		$scope.create = function() {
			// Create new Recipe object
			var recipe = new Recipes ($scope.recipe);

			// Redirect after save
			recipe.$save(function(response) {
				$location.path('recipes/' + response._id);

				// Clear form fields
				$scope.name = '';
			}, function(errorResponse) {
				console.error(errorResponse);
				$scope.error = errorResponse.data.message;
			});
		};

		// Remove existing Recipe
		$scope.remove = function(recipe) {
			if ( recipe ) { 
				recipe.$remove();

				for (var i in $scope.recipes) {
					if ($scope.recipes [i] === recipe) {
						$scope.recipes.splice(i, 1);
					}
				}
			} else {
				$scope.recipe.$remove(function() {
					$location.path('recipes');
				});
			}
		};

		// Update existing Recipe
		$scope.update = function() {
			var recipe = $scope.recipe;
			console.info('Updating the recipe on server.');
			recipe.$update(function() {
				$location.path('recipes/' + recipe._id);
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		// Find a list of Recipes
		$scope.find = function() {
			$scope.recipes = Recipes.query();
			console.debug($scope.recipes);
		};

		// Find existing Recipe
		$scope.findOne = function() {
			$scope.recipe = Recipes.get({ 
				recipeId: $stateParams.recipeId
			});

			//Get all your Ingredients
			$scope.ingredientMaster = Products.getAll();

			//ToDo Get all the base recipe master
			$scope.baseRecipeMaster = Recipes.query();
			console.debug($scope.baseRecipeMaster);
			//console.debug($scope.recipe);
		};

		$scope.editInit = function(){
			$scope.recipe = Recipes.get({
				recipeId: $stateParams.recipeId
			});

			//Wait for the recipe to be resolved and then make the necessary schema data change
			$scope.recipe.$promise.then(function(data){
				for(var i in data.ingredientItems){
					data.ingredientItems[i].ingredient = data.ingredientItems[i].ingredient._id;
				}
				for(var j in data.baseRecipeItems){
					data.baseRecipeItems[j].recipe = data.baseRecipeItems[j].recipe._id;
				}
			});

			//Get all your Ingredients
			$scope.ingredientMaster = Products.getAll();

			//ToDo Get all the base recipe master
			$scope.baseRecipeMaster = Recipes.query();
			console.debug($scope.baseRecipeMaster);
			//console.debug($scope.recipe);

		};
	}
]);
