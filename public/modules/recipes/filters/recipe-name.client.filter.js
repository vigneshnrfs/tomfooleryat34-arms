'use strict';

angular.module('recipes').filter('recipeName', [
	function() {
		return function(input) {
			// Recipe name directive logic
			// ...

			return 'recipeName filter: ' + input;
		};
	}
]);