'use strict';

/**
 * Module dependencies.
 */
var _ = require('lodash');

/**
 * Extend user's controller
 */
module.exports = _.extend(
	require('./pos/pos.menuitems.server.controller'),
	require('./pos/pos.groupcode.server.controller')

);
