/**
 * Created by Vignesh on 20/06/2015.
 */
'use strict';

var mongoose = require('mongoose'),

    MenuItem = mongoose.model('MenuItem.pos'),

    pos = require('../lib/pos.interface'),
    _ = require('lodash'),
    async = require('async');


function getDoc(id, callback) {
    MenuItem.findById(id).populate('portions.activeRecipe').exec(function (err, menuItem) {
        if (err)return callback(err);
        return callback(null, menuItem);
    });
}

exports.CreateMenuItemOnServer = function (id, callback) {
    getDoc(id, function (err, menuItem) {
        if (err)return callback(err);
        pos.CreateMenuItemToPOS(menuItem, function (err, menuItem) {
            if (err) return callback(err);
            menuItem.save(function (err) {
                if (err)return callback(err);
                return callback(null, menuItem);
            });
        });
    });
};


exports.UpdateMenuItemOnServer = function (id, callback) {

    getDoc(id, function (err, menuItem) {
        if (err)return callback(err);

        pos.UpdateMenuItemToPOS(menuItem, function (err, menuItem) {

            if (err) return callback(err);
            return callback(null, menuItem);
        });
    });
};


exports.syncFromServer = function (callback) {
    /**
     * Connecct to the SQL Server and returns the sql client for queries.
     * @param {function} callback Callback function
     */
    var connectToServer = function (callback) {
        console.log('Connecting to Server');
        pos.connectToServer(function (err, client) {
            if (err) return callback(err);
            return callback(null, client);
        });
    };

    var getMenuItemPortions = function (client, callback) {
        var request = new client.Request();
        var qry = 'SELECT MenuItemPortions.Id as posId, MenuItemPortions.Name as name, MenuItemPrices.Price as price, ' +
            'MenuItemPortions.MenuItemId as menuItemId ' +
            'From MenuItemPortions ' +
            'INNER JOIN MenuItemPrices ON MenuItemPrices.MenuItemPortionId=MenuItemPortions.Id';
        //'WHERE MenuItemPortions.MenuItemId=' + menuItemId;
        //console.log(qry);
        request.query(qry, function (err, recordset) {
            if (err) return callback(err);
            return callback(null, recordset);
        });
    };

    var getMenuItemsFromPOS = function (client, callback) {
        console.log('getting MenuItems from POS');
        var request = new client.Request();
        var qry = 'SELECT Id as posId, GroupCode as groupCode, Name as name FROM MenuItems';
        console.log(qry);
        request.query(qry, function (err, recordset) {
            if (err) return callback(err);
            if (recordset.length > 0) {
                return callback(null, recordset);
            }
        });
    };

    var updateMenuItems = function (menuItems, callback) {
        var crudPortion = function (menuItem, data, callback) {
            //Reiterate through portion and add/update/delete accordingly.
            async.each(menuItem.portions, function (portion, callback) {
                    portion.isCurrent = true;
                    //var temp = _.find(data.portions, {'meta.posId': portion.meta.posId});
                    //console.log('PORTIONS ', data.portions) ;
                    //console.log('POS Data ', portion.meta.posId);
                    var temp = _.find(data.portions, {'meta': {'posId': portion.meta.posId}});
                    //console.log(temp);
                    if (temp) {
                        //Portion Exist. Just update the name & price
                        temp.name = portion.name;
                        temp.price = portion.price;
                        temp.isCurrent = true;
                        return callback();
                    } else {
                        //Portion does not exist. Create a new one.
                        data.portions.push(portion);
                        return callback();
                    }

                }, function () {
                    _.remove(data.portions, function (portion) {
                        if (portion.isCurrent) {
                            delete portion.isCurrent;
                            return false;
                        } else {
                            return true;
                        }
                    });
                    return callback();

                }
            );

        };

        async.each(menuItems, function (menuItem, callback) {

            MenuItem.findOne({'meta.posId': menuItem.meta.posId}).exec(function (err, data) {
                if (err) return callback(err);

                if (data) {
                    //Menu Item exits. Update the menu name and add/update portion types.
                    // console.log('Menu Item Exist. Updating the MenuItem');
                    data.groupCode = menuItem.groupCode;
                    data.name = menuItem.name;
                    /*Create Update Delete the Portions */
                    crudPortion(menuItem, data, function () {
                        //CRUD Done. Save Data
                        //console.log(data);
                        data.save(function (err) {
                            if (err) return callback(err);
                            return callback();
                        });
                    });
                } else {
                    //Menu Item does not exist. Create a new one.
                    //console.log('MenuItem does not exist. Creating a new one.');
                    data = new MenuItem({
                        name: menuItem.name,
                        groupCode: menuItem.groupCode,
                        meta: {posId: menuItem.meta.posId},
                        portions: menuItem.portions
                    });

                    data.save(function (err) {
                        if (err) {
                            // console.log(data);
                            console.log(err);
                            return callback(err);
                        }
                        return callback();
                    });
                }

            });
        }, function (err) {
            if (err) return callback(err);
            return callback();
        });

    };

    var execute = function (callback) {
        console.log('--- Synchronising MenuItems from POS ---');
        async.waterfall([
                //Connect to server
                function (callback) {
                    connectToServer(function (err, client) {
                        if (err) return callback(err);
                        return callback(null, client);
                    });
                },
                /* get all the menu items */
                function (client, callback) {

                    getMenuItemsFromPOS(client, function (err, result) {
                        if (err) return callback(err);
                        return callback(null, client, result);
                    });
                },
                /* Get all menuItem portions */
                function (client, menuItems, callback) {

                    getMenuItemPortions(client, function (err, menuItemPortions) {
                        if (err) return callback(err);
                        return callback(null, menuItems, menuItemPortions);
                    });
                },
                /* Map MenuItems to Portions */
                function (menuItems, menuItemPortions, callback) {

                    //console.log('menuItem',menuItems);
                    //console.log('menuItemPortions', menuItemPortions);

                    async.forEach(menuItems,
                        function (menuItem, callback) {
                            menuItem.meta = {};
                            menuItem.meta.posId = menuItem.posId;
                            delete menuItem.posId;
                            menuItem.portions = [];
                            menuItem.portions = _.filter(menuItemPortions, {'menuItemId': menuItem.meta.posId});

                            //Manipulate the object to match mongo model
                            async.forEach(menuItem.portions, function (portion, callback) {
                                portion.meta = {'posId': portion.posId};
                                delete portion.posId;
                                delete portion.menuItemId;
                                callback();
                            }, function () {
                                return callback();
                            });

                        },
                        function (err) {
                            if (err) return callback(err);
                            return callback(null, menuItems);
                        });
                },
                /* Find & Update local database */
                function (menuItems, callback) {
                    updateMenuItems(menuItems, function (err) {
                        if (err) return callback(err);
                        return callback(null, menuItems);
                    });
                }
            ],
            //Waterfall Callback Function
            function (err, result) {
                if (err) return callback(err);
                return callback(null, result);
            }
        );
    };

    var testOne = function (callback) {
        MenuItem.find({activeRecipe: {$exists: false}}).remove(function () {
            return callback();
        });
    };


    execute(function (err, result) {
        if (err) return callback(err);
        return callback(null, result);
    });


};
