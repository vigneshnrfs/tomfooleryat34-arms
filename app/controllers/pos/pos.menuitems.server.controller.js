'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	errorHandler = require('../errors.server.controller'),
	MenuItem = mongoose.model('MenuItem.pos'),
	GroupCode = mongoose.model('GroupCode.pos'),
	pos = require('./sync/pos.sync'),
	_ = require('lodash');




/**
 * Create a MenuItem
 */

exports.create = function(req, res) {
	var menuItem = new MenuItem(req.body);
	menuItem.user = req.user;
	menuItem.save(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			pos.CreateMenuItemOnServer(menuItem._id,function(err,menuItem){
				if(err){
					return res.status(400).send({
						message: errorHandler.getErrorMessage(err)
					});
				} else{
					res.jsonp(menuItem);
				}
			});

		}
	});




};

/**
 * Show the current MenuItem
 */
exports.read = function(req, res) {
	res.jsonp(req.menuItem);
};

/**
 * Update a MenuItem
 */
exports.update = function(req, res) {
	console.log('Updating Menu Item');
	var menuItem = req.menuItem ;
	menuItem = _.extend(menuItem , req.body);
	menuItem.save(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			pos.UpdateMenuItemOnServer(menuItem._id,function(err,menuItem){
				if(err){
					console.error(err);
					return res.status(400).send({
						message: errorHandler.getErrorMessage(err)
					});
				} else{
					res.jsonp(menuItem);
				}
			});
		}
	});
};

/**
 * Delete an MenuItem
 */
exports.delete = function(req, res) {
	var menuItem = req.menuItem;

	menuItem.remove(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(menuItem);
		}
	});
};

/**
 * List of Menuitems
 */
exports.list = function(req, res) { 
	MenuItem.find().sort('-created').populate('user', 'displayName').exec(function(err, menuItems) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(menuItems);
		}
	});
};

/**
 * MenuItem middleware
 */
exports.menuItemByID = function(req, res, next, id) { 
	MenuItem.findById(id).populate('portions.activeRecipe','name').exec(function(err, menuItem) {
		if (err) return next(err);
		if (! menuItem) return next(new Error('Failed to load MenuItem ' + id));
		req.menuItem = menuItem ;
		next();
	});
};

/**
 * MenuItem authorization middleware
 */
exports.hasAuthorization = function(req, res, next) {
	if (req.menuItem.user.id !== req.user.id) {
		return res.status(403).send('User is not authorized');
	}
	next();
};


