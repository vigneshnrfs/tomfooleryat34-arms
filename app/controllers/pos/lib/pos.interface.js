/**
 * Created by Vignesh on 19/06/2015.
 */
'use strict';

var sql = require('mssql');
var _ = require('lodash');
var async = require('async');
var pos = _.extend(
    require('./create.menuitems'),
    require('./update.menuitems'),
    require('./import.tickets')
);




var config = require('../../../../config/config').dbConfig;


exports.connectToServer =function(callback) {
    //var sqlClient = new sql.Connection();

    sql.connect(config, function (err) {
        if (err) {
            console.log('Error occurred while connecting to SAMBAPOS.');
            return callback(err);
        } else {
            console.log('SAMBAPOS Database Connection Successful....');
            return callback(null, sql);
        }
    });
};


exports.CreateMenuItemToPOS = function (menuItem, callback) {
    var data = menuItem;

    //Step: 1 Make Connection to Server.

    exports.connectToServer(function (err, sqlClient) {
        if (err)return callback(err);
        var t = new sqlClient.Transaction();
        t.begin(function(err){
            if(err) return callback(err);
            //Step 2: Create the Menu Item
            pos.CreateMenuItem(sqlClient, data, function (err, menuItemId) {
                if (err)return callback(err);
                data.meta.posId = menuItemId;
                //Step 3: Create portions.
                async.each(data.portions,
                    function (item, callback) {
                        pos.CreateMenuPortion(sqlClient, data.meta.posId, item.name, item.price, function (err, menuItemPortionId) {
                            if (err) return callback(err);
                            item.meta.posId = menuItemPortionId;
                            //Step 4: Create Recipe if exists
                            //TODO Come up with a better idea to check if the price exists.
                            if (item.cost) {
                                pos.CreateRecipeCost(sqlClient, menuItemPortionId, item.activeRecipe.name, item.cost, function (err) {
                                    if (err)return callback(err);
                                    return callback();
                                });
                            } else {
                                return callback();
                            }
                        });
                    },
                    function (err) {
                        if (err)return callback(err);
                        t.commit(function(err){
                            if(err)return callback(err);
                            return callback(null, data);
                        });
                    }
                );
            });
        });


    });


};

exports.UpdateMenuItemToPOS = function (menuItem, callback) {
    var data = menuItem;


    //Step: 1 Make Connection to Server.

    exports.connectToServer(function (err, sqlClient) {
        if (err)return callback(err);
        var t = new sqlClient.Transaction();

        t.begin(function (err) {
            if (err)return callback(err);
            //Step 2: Create the Menu Item
            pos.UpdateMenuItem(sqlClient, data, function (err) {
                if (err)return callback(err);
                //Step 3: Create portions.
                async.each(data.portions,
                    function (item, callback) {

                        pos.UpdateMenuPortion(sqlClient, item.meta.posId, item.name, item.price, function (err) {
                            if (err) return callback(err);

                            //Step 4: Create Recipe if exists
                            //TODO Come up with a better idea to check if the price exists.

                            if (item.cost) {
                                pos.UpdateRecipeCost(sqlClient, item.meta.posId, item.activeRecipe.name, item.cost, function (err) {
                                    if (err)return callback(err);
                                    return callback();

                                });
                            } else {
                                return callback();
                            }
                        });
                    },
                    function (err) {
                        if (err)return callback(err);
                        t.commit(function(err){
                            if(err) return callback(err);
                            return callback(null, data);
                        });
                    }
                );
            });
        });


    });
};

exports.getSalesFromPOS = function(fromDate,toDate,callback){
    exports.connectToServer(function(err,sql){
        if(err)return callback(err);
        pos.importTickets(sql,fromDate,toDate,function(err,data){
            if (err) return callback(err);
            return callback(null,data);
        });
    });
};



exports.getCovers = function(startDate,endDate,callback){
    exports.connectToServer(function(err,sql){
        if(err)return callback(err);
        pos.getCovers(startDate,endDate,sql,function(err,data){
            if(err)return callback(err);

            //Parse Json
            async.each(data,function(ticket,callback){
                var str = ticket.TicketTags;
                if(str){

                    //Remove the brackets. Otherwise JSON.parse throws an error.
                    str = str.split('[').join('');
                    str = str.split(']').join('');
                    ticket.TicketTags = JSON.parse(str);
                    ticket.TicketTags.TV = parseInt(ticket.TicketTags.TV);
                }

                callback();
            },function(err){
                if(err) return callback(err);
                return callback(null,data);
            });
        });
    });
};
