/**
 * Created by Vignesh on 20/06/2015.
 */
'use strict';

exports.CreateMenuItem = function (sql, data, callback) {

    var request = new sql.Request();
    //Create the menu
    request.query('SELECT Id FROM MenuItems WHERE Name= \'' + data.name + '\'', function (err, recordset) {
        if (err) return callback(err);
        if (recordset.length > 0) {
            return callback('The Menu Item already exists in the POS.');
        } else {

            //INSERT the MenuItem
            var qryString = 'INSERT INTO MenuItems (GroupCode,Name) VALUES (\'' + data.groupCode + '\',\'' + data.name + '\')';
            request.query(qryString, function (err, recordset) {
                if (err) return callback(err);
                request.query('SELECT MAX(Id) as maxid FROM MenuItems', function (err, recordset) {
                    if (err) return callback(err);
                    var menuItemId = recordset[0].maxid;
                    return callback(null, menuItemId);
                });
            });
        }
    });
};

exports.CreateMenuPortion = function (sql, menuItemId, portionName, price, callback) {
    var request = new sql.Request();
    var qryString = 'INSERT INTO MenuItemPortions (Name,MenuItemId,Multiplier) VALUES(\'' + portionName + '\',\'' + menuItemId + '\',\'1\')';

    request.query(qryString, function (err, recordset) {
        if (err)return callback(err);
        request.query('SELECT Id as MenuItemPortionId FROM MenuItemPortions WHERE MenuItemId=' + menuItemId + 'AND Name=\'' + portionName + '\'', function (err, recordset) {
            if (err) return callback(err);
            var MenuItemPortionId = recordset[0].MenuItemPortionId;
            request.query('INSERT INTO MenuItemPrices(MenuItemPortionId,Price) VALUES(\'' + MenuItemPortionId + '\',\'' + price + '\')', function (err, recordset) {
                if (err) return callback(err);
                return callback(null, MenuItemPortionId);
            });
        });
    });
};


/**
 * Create Recipe Costs in the SamabaServer
 * @param sql
 * @param MenuItemPortionId
 * @param RecipeName
 * @param Cost
 * @param callback
 */
exports.CreateRecipeCost = function (sql, MenuItemPortionId, RecipeName, Cost, callback) {
    //Check for existing recipe. Needed this because sometime the recipes may aleady esixt on the pos.
    console.info('Calling CreateRecipeCost()');
    var request = new sql.Request();
    var qry = 'SELECT id from Recipes WHERE Portion_Id=\'' + MenuItemPortionId + '\'';
    request.query(qry, function (err, recordset) {
        if (err)return callback(err);
        var request;
        if(recordset.length >0){

            //Recipe exist. Just update the costs.
            request = new sql.Request();
            var qry = 'UPDATE Recipes SET FixedCost=\'' + Cost + '\',Name=\'' + RecipeName + '\' WHERE Portion_Id=\'' + MenuItemPortionId + '\'';
            request.query(qry, function (err, recordset) {
                if (err)return callback(err);
                return callback();
            });

        } else{

            //Recipe Does not exist. So create new recipe.
            request = new sql.Request();
            request.query('INSERT INTO Recipes(FixedCost,Name,Portion_Id) VALUES(\'' + Cost + '\',\'' + RecipeName + '\',\'' + MenuItemPortionId + '\')', function (err) {
                if (err)return callback(err);
                return callback();
            });
        }
    });
};
