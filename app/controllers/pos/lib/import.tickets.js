'use strict';


var _ = require('lodash');
var pos = _.extend(
    require('./pos.interface')
);


exports.getCovers = function(startDate,endDate,sql,callback){
    console.log('Calling getCovers');
    var request = new sql.Request();

    var qry = 'SELECT Tickets.Id as TicketId, Tickets.TotalAmount, Tickets.TicketTags, ' +
        'TicketEntities.EntityId,TicketEntities.EntityName, EntityScreenItems.EntityScreenId, EntityScreens.Name as FloorName ' +
        'FROM Tickets ' +
        'LEFT JOIN TicketEntities ON Tickets.Id = TicketEntities.Ticket_Id ' +
        'LEFT JOIN EntityScreenItems ON TicketEntities.EntityId = EntityScreenItems.EntityId ' +
        'LEFT JOIN EntityScreens ON EntityScreens.Id = EntityScreenItems.EntityScreenId ' +
        'WHERE Tickets.Date > \''+ startDate.toISOString() + '\' AND Tickets.Date < \'' + endDate.toISOString() + '\' AND EntityScreens.Id BETWEEN 4 AND 6';
    //console.log(qry);
    request.query(qry,function(err,recordset){

        if(err){
            console.log('Error Occurred',err);
            return callback(err);
        }
        return callback(null,recordset);

    });




};

exports.importTickets = function(sql,fromDate,toDate,callback){
    console.log('Calling Import Tickets...');
    var request = new sql.Request();

    var qry = 'SELECT MenuItemName, sum(Quantity)as Quantity FROM Orders GROUP BY MenuItemName';
    var q = 'SELECT Orders.MenuItemName, Orders.PortionName, Orders.Price, SUM(Orders.Quantity) AS Quantity,' +
        ' AVG(CostItems.CostPrediction) AS CostPrediction, AVG(CostItems.Cost) AS Cost' +
        ' FROM Orders INNER JOIN CostItems ON Orders.MenuItemName = CostItems.Name AND Orders.PortionName = CostItems.PortionName' +
        ' INNER JOIN PeriodicConsumptions ON CostItems.PeriodicConsumptionId = PeriodicConsumptions.Id ' +
        '    GROUP BY Orders.MenuItemName, Orders.PortionName, Orders.Price';

    var q1 = 'SELECT MenuItems.GroupCode,Orders.MenuItemName, Orders.PortionName, Orders.Price, sum(Orders.Quantity) as Quantity ' +
        'FROM Orders ' +
        'INNER JOIN MenuItems ON Orders.MenuItemId = MenuItems.Id ' +
        'LEFT JOIN Tickets ON Orders.TicketId = Tickets.Id ' +
        'WHERE Orders.CalculatePrice = 1 AND Tickets.Date > \''+ fromDate.toISOString() + '\' AND Tickets.Date < \'' + toDate.toISOString() + '\' '+
        'GROUP BY MenuItems.GroupCode, Orders.MenuItemName, Orders.PortionName, Orders.Price';

    var q2 = 'SELECT Orders.TicketId,Orders.CreatedDateTime,MenuItems.GroupCode,Orders.MenuItemName, Orders.PortionName, Orders.Price, Orders.Quantity ' +
        'FROM Orders ' +
        'INNER JOIN MenuItems ON Orders.MenuItemId = MenuItems.Id ' +
        'LEFT JOIN Tickets ON Orders.TicketId = Tickets.Id ' +
        'WHERE Orders.CalculatePrice = 1 AND Orders.DecreaseInventory = 1 AND Tickets.Date > \''+ fromDate + '\' AND Tickets.Date < \'' + toDate + '\'';
    var q3 = 'SELECT sum(TotalAmount) from Tickets ' +
        'WHERE Tickets.isClosed = 1 AND Tickets.Date > \''+ fromDate + '\' AND Tickets.Date < \'' + toDate + '\' ';

//    console.log(q3);

    request.query(q1,function(err,recordset){
       if(err){
           console.log('Error Occurred',err);
           return callback(err);
       }
        //debugger;
        callback(null, recordset);
    });


};

