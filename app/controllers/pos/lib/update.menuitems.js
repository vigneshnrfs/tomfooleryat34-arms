/**
 * Created by Vignesh on 20/06/2015.
 */
'use strict';

exports.UpdateMenuItem = function (sql, data, callback) {
    console.info('Calling UpdateMenuItem()');
    var request = new sql.Request();
    var qryString = 'UPDATE MenuItems SET Name=\'' + data.name + '\', GroupCode=\'' + data.groupCode + '\' WHERE Id=\'' + data.meta.posId + '\'';
    request.query(qryString, function (err) {
        if (err) return callback(err);
        return callback();
    });
};

exports.UpdateMenuPortion = function (sql, id, portionName, price, callback) {
    console.info('Calling UpdateMenuPortion()');
    //Checking if the Recipe Exist in the database

    var request = new sql.Request();
    var qry = 'UPDATE MenuItemPortions SET Name=\'' + portionName + '\',Multiplier=\'1\' WHERE Id=\'' + id + '\'';
    request.query(qry, function (err, recordset) {
        if (err)return callback(err);
        var qry1 = 'UPDATE MenuItemPrices SET Price=\'' + price + '\' WHERE MenuItemPortionId=\'' + id + '\'';
        request.query(qry1, function (err) {
            if (err)return callback(err);
            return callback();
        });
    });
};


exports.UpdateRecipeCost = function (sql, MenuItemPortionId, RecipeName, Cost, callback) {

    console.info('Calling UpdateRecipeCost()');
    var request = new sql.Request();
    var qry = 'SELECT id from Recipes WHERE Portion_Id=\'' + MenuItemPortionId + '\'';
    request.query(qry, function (err, recordset) {
        if (err)return callback(err);
        var request;
        if(recordset.length >0){

            //Recipe exist. Just update the costs.
            request = new sql.Request();
            var qry = 'UPDATE Recipes SET FixedCost=\'' + Cost + '\',Name=\'' + RecipeName + '\' WHERE Portion_Id=\'' + MenuItemPortionId + '\'';
            request.query(qry, function (err, recordset) {
                if (err)return callback(err);
                return callback();
            });

        } else{

            //Recipe Does not exist. So create new recipe.
            request = new sql.Request();
            request.query('INSERT INTO Recipes(FixedCost,Name,Portion_Id) VALUES(\'' + Cost + '\',\'' + RecipeName + '\',\'' + MenuItemPortionId + '\')', function (err) {
                if (err)return callback(err);
                return callback();
            });
        }
    });

};
