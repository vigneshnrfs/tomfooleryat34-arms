'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    errorHandler = require('./../errors.server.controller.js'),
    mailjet = require('./../mailjet.server.controller.js'),
    Report = mongoose.model('Report'),
    _ = require('lodash'),
    async = require('async');

//var sambapos = require('../../app/controllers/sambapos.server.controller');
var pos = require('./lib/pos.interface');

var getPosSales = function (fromDate, toDate, callback) {
    pos.getSalesFromPOS(fromDate, toDate, function (err, data) {
        if (err) {
            return callback(err);
        } else {
            callback(null, data);
        }
    });
};

/**
 *
 * @param arr
 * @param callback
 */
var getFloorCountAndTotals = function (arr, callback) {
    async.parallel([
            function (callback) {
                if(arr){
                    async.reduce(arr, 0, function (memo, item, callback) {
                        if (item.TicketTags) {
                            if (item.TicketTags.TV) {
                                callback(null, memo + item.TicketTags.TV);
                            }
                        } else {
                            callback(null, memo);
                        }
                    }, function (err, result) {
                        // result is now equal to the last value of memo, which is 6
                        callback(null, result);
                    });
                } else{
                    callback(null,0);
                }

            },
            function (callback) {
                if(arr){
                    async.reduce(arr, 0, function (memo, item, callback) {
                        callback(null, memo + item.TotalAmount);
                    }, function (err, result) {
                        callback(null, result);
                    });
                } else {
                    callback(null,0);
                }

            }
        ],
        function (err, data) {
            if (err) return callback(err);
            console.log(data);
            callback(null, {covers: data[0], sales: data[1]});
        }
    );
};

var getTicketDetails = function(fromDate,toDate,callback){
    pos.getCovers(fromDate, toDate, function (err, data) {
        if (err) return callback(err);

        //process the data:
        var floors = _.groupBy(data, 'FloorName');

        async.parallel({
            firstFloor: function (callback) {
                getFloorCountAndTotals(floors['First Floor'], function (err, results) {
                    if (err) return callback(err);
                    callback(err, results);
                });
            },
            groundFloor: function (callback) {
                getFloorCountAndTotals(floors['Ground Floor'], function (err, results) {
                    if (err) return callback(err);
                    callback(err, results);
                });
            },
            barTab: function (callback) {
                getFloorCountAndTotals(floors['Bar Tab'], function (err, results) {
                    if (err) return callback(err);
                    callback(err, results);
                });
            }
        }, function (err, results) {
            //callback function
            if(err) return callback(err);
            return callback(null,results);
        });

    });
};

exports.processSalesData = function(fromDate,toDate,callback){
    async.series({
            salesDetail: function(callback){
                getPosSales(fromDate,toDate,function(err,results){
                    if(err) return callback(err);
                   // console.log('Results from sales...',results);
                    return callback(null,results);
                });
            },
            floorCountAndTotals: function(callback){
                getTicketDetails(fromDate,toDate,function(err,results){
                    if(err) return callback(err);
                    //console.log('Floor count and totals',results);
                    callback(null,results);
                });
            }
        },
        function (err, results) {
            if(err) return callback(err);
            //console.log('Final Results',results);
            callback(null,results);
        }
    );
};

exports.sales = function (req, res) {
    var fromDate = new Date(req.query.fromDate);
    var toDate = new Date(req.query.toDate);
    console.log('Getting sales date between '+ fromDate + ' to ' + toDate);
    exports.processSalesData(fromDate,toDate,function(err,results){
        if(err) return res.status(404).send(err);
        res.jsonp(results);
    });
};

exports.hasAuthorization = function (req, res, next) {
    if (req.report.user.id !== req.user.id) {
        return res.status(403).send('User is not authorized');
    }
    next();
};
