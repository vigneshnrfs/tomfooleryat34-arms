'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	errorHandler = require('../errors.server.controller'),
	GroupCode = mongoose.model('GroupCode.pos'),
	_ = require('lodash');



/**
 * Create a GroupCode
 */

exports.groupCode = {};
exports.groupCode.create = function(req,res){
	var groupCode = new GroupCode(req.body);


	groupCode.save(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(GroupCode);
		}
	});
};

/**
 * List of GroupCodes
 */
exports.groupCode.list = function(req, res) {

	GroupCode.find().sort('-created').exec(function(err, groupCodes) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(groupCodes);
		}
	});
};

/**
 *READ GroupCode
 */
exports.groupCode.read = function(req, res) {
	res.jsonp(req.groupCode);
};


/**
 * Update a Group Code
 */
exports.groupCode.update = function(req, res) {
	var groupCode = req.groupCode ;

	groupCode = _.extend(groupCode , req.body);

	groupCode.save(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(groupCode);
		}
	});
	//TODO Update the GroupCodes in POS.
};

/**
 * Delete an GroupCode
 */
exports.groupCode.delete = function(req, res) {
	var groupCode = req.groupCode;

	groupCode.remove(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(groupCode);
		}
	});
};



/**
 * MenuItem middleware
 */
exports.groupCode.groupCodeByID = function(req, res, next, id) {
	GroupCode.findById(id).exec(function(err, groupCode) {
		if (err) return next(err);
		if (! groupCode) return next(new Error('Failed to load groupcode ' + id));
		req.groupCode = groupCode ;
		next();
	});
};

/**
 * MenuItem authorization middleware
 */
exports.hasAuthorization = function(req, res, next) {
	if (req.menuItem.user.id !== req.user.id) {
		return res.status(403).send('User is not authorized');
	}
	next();
};
