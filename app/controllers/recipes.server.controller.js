'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    errorHandler = require('./errors.server.controller'),
    Recipe = mongoose.model('Recipe'),
    Supplier = mongoose.model('Supplier'),
    MenuItem = mongoose.model('MenuItem.pos'),
    pos = require('./pos/sync/pos.sync'),
    async = require('async'),
    _ = require('lodash');

/**
 * Create a Recipe
 */
exports.create = function (req, res) {
    var recipe = new Recipe(req.body);
    recipe.user = req.user;

    recipe.save(function (err) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.jsonp(recipe);
        }
    });
};

/**
 * Show the current Recipe
 */
exports.read = function (req, res) {
    res.jsonp(req.recipe);
};

/**
 * Update a Recipe
 */
exports.update = function (req, res) {
    var recipe = req.recipe;
    recipe = _.extend(recipe, req.body);

    recipe.save(function (err) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.jsonp(recipe);
        }

        var menuItemsToUpdate;
        //Search for base Recipes and change the costs.
        async.series([
                function (callback) {
                    Recipe.find({'baseRecipeItems.recipe': recipe._id}).exec(function (err, baseRecipes) {
                        menuItemsToUpdate = _.pluck(baseRecipes,'_id');

                        async.each(baseRecipes,
                            function (baseRecipe, callback) {
                                async.each(baseRecipe.baseRecipeItems,
                                    function (item, callback) {
                                        if (item.recipe.equals(recipe._id)) {
                                            //Its a match. Update the costs.
                                            item.unitCost = recipe.unitCost;
                                            item.totalCost = recipe.unitCost * item.quantity;
                                            baseRecipe.save(function (err) {
                                                if (err) {
                                                    console.error(err);
                                                    return callback(err);

                                                }else {
                                                    return callback(null);
                                                }
                                            });
                                        } else {
                                            return callback(null);
                                        }

                                    },
                                    function (err) {
                                        return callback(null);
                                    }
                                );
                            }, function () {
                                return callback(null);
                            }
                        );
                    });
                },
                function (callback){
                    menuItemsToUpdate.push(recipe._id);
                    callback();
                },
                //Search for all the menu items that needs to be changed.
                function (callback) {
                    console.log('Menu Items to Update = ', menuItemsToUpdate);
                    async.each(menuItemsToUpdate,
                        function(recipeId,callback){
                            MenuItem.find({'portions.activeRecipe': recipeId}).exec(function (err, menuItems) {
                                console.log(menuItems.length);
                                async.each(menuItems,
                                    function (menuItem, callback) {
                                        console.log('Matched Menu Item Name = ', menuItem.name);
                                        async.each(menuItem.portions,
                                            function (portion, callback) {
                                                if (portion.activeRecipe.equals(recipeId)) {
                                                    console.log('Recipe Matched');
                                                    portion.cost = recipe.portionCost;
                                                    menuItem.save(function (err) {
                                                        //Update the menuitem on Sambapos now.
                                                        pos.UpdateMenuItemOnServer(menuItem._id, function (err, menuItem) {
                                                            return callback();

                                                        });
                                                    });
                                                }else {
                                                    return callback();
                                                }
                                            }, function (err) {
                                                return callback();
                                            });
                                    },
                                    function (err) {
                                        return callback();
                                    }
                                );
                            });

                        },function(err){
                            return callback();
                        }
                    );

                }
            ],
            function (err) {
                console.log('All Updates successful');
            }
        );


    });
};

/**
 * Delete an Recipe
 */
exports.delete = function (req, res) {
    var recipe = req.recipe;
    //TODO Validate that the recipe is an orphan and not attached to any MenuITem
    recipe.remove(function (err) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.jsonp(recipe);
        }
    });
};


/**
 * List of Recipes
 */
exports.list = function (req, res) {
    Recipe.find().sort('name').populate('user', 'displayName').exec(function (err, recipes) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.jsonp(recipes);
        }
    });
};

/**
 * Recipe middleware
 */
exports.recipeByID = function (req, res, next, id) {
    Recipe.findById(id).populate('ingredientItems.ingredient', 'name supplier').populate('baseRecipeItems.recipe', 'name').exec(function (err, recipe) {
        if (err) return next(err);
        if (!recipe) return next(new Error('Failed to load Recipe ' + id));

        Supplier.populate(recipe, {path: 'ingredientItems.ingredient.supplier', select: 'name'}, function (err, doc) {
            if (err) return next(err);
            req.recipe = recipe;
            next();
        });
    });
};

/**
 * Recipe authorization middleware
 */
exports.hasAuthorization = function (req, res, next) {
    if (req.recipe.user.id !== req.user.id) {
        return res.status(403).send('User is not authorized');
    }
    next();
};
