'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    errorHandler = require('./../errors.server.controller.js'),
    Checklist = mongoose.model('OpeningChecklistTemplate'),
    OpeningChecklist = mongoose.model('OpeningChecklist'),
    OpeningCLCtrl = require('./opening.checklists.server.controller.js'),
    async= require('async'),
    _ = require('lodash');

exports.oneTime = function(req,res){
  Checklist.find().exec(function(err,docs){
      async.each(docs,function(doc,callback){

          var checklist = new Checklist({group:doc.group,
          taskName:doc.taskName,shift:'night',user:req.user});
          checklist.shift='night';
          checklist.save(function(err){
              if (err) return callback(err);
              return callback();
          });
      },function(err){
          if(err){
              return res.status(400).send(err);
          }else{
              res.send('OK');
          }
      });
  });
};


/**
 * Create a Checklist
 */
exports.create = function (req, res) {
    var checklist = new Checklist(req.body);
    checklist.user = req.user;

    checklist.save(function (err) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            var date;
            if(checklist.shift === 'day'){
                date = new Date().setHours(8, 0, 0, 0);
            } else {
                date = new Date().setHours(17, 0, 0, 0);
            }

            OpeningChecklist.find({'date': date}).exec(function (err, doc) {

                if (doc.length === 0) {
                    var checklistObj = new OpeningChecklist({date: date});
                    checklistObj.checklists.push({name: checklist.taskName, group: checklist.group});
                    checklistObj.save(function (err) {
                        if(err) console.error(err);
                    });
                } else {
                    doc[0].checklists.push({name: checklist.taskName, group: checklist.group});
                    doc[0].save(function (err) {
                        if(err) console.error(err);
                    });
                }
            });
            res.jsonp(checklist);
        }
    });
};

/**
 * Show the current Checklist
 */
exports.read = function (req, res) {
    res.jsonp(req.checklist);
};

/**
 * Update a Checklist
 */
exports.update = function (req, res) {
    var checklist = req.checklist;

    checklist = _.extend(checklist, req.body);

    checklist.save(function (err) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.jsonp(checklist);
        }
    });
};

/**
 * Delete an Checklist
 */
exports.delete = function (req, res) {
    var checklist = req.checklist;

    checklist.remove(function (err) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.jsonp(checklist);
        }
    });
};

/**
 * List of Checklists
 */
exports.list = function (req, res) {
    Checklist.find({'shift':req.query.shift}).sort('-created').populate('user', 'displayName').exec(function (err, checklists) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.jsonp(checklists);
        }
    });
};

/**
 * Checklist middleware
 */
exports.checklistTaskByID = function (req, res, next, id) {
    Checklist.findById(id).populate('user', 'displayName').exec(function (err, checklist) {
        if (err) return next(err);
        if (!checklist) return next(new Error('Failed to load Checklist ' + id));
        req.checklist = checklist;
        next();
    });
};

/**
 * Checklist authorization middleware
 */
exports.hasAuthorization = function (req, res, next) {
    if (req.checklist.user.id !== req.user.id) {
        return res.status(403).send('User is not authorized');
    }
    next();
};
