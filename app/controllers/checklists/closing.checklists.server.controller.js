'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    errorHandler = require('./../errors.server.controller.js'),
    Checklist = mongoose.model('ClosingChecklist'),
    ClosingChecklistTemplate = mongoose.model('ClosingChecklistTemplate'),
    async = require('async'),
    mailjet = require('./../mailjet.server.controller.js'),
    _ = require('lodash');
/**
 * Populate the daily checklist from the template.
 * @param date
 * @param callback
 */
exports.createFromTemplate = function (date, callback) {
    var importTasks = function (tempChecklists, date, callback) {
        var checklistObj = new Checklist();
        checklistObj.date = date;
        var tmpArray = [];

        async.each(tempChecklists,
            function (temp, callback) {
                tmpArray.push({name: temp.taskName, group: temp.group});

                callback();
            }, function (err) {
                if (err) return callback(err);
                checklistObj.checklists = tmpArray;
                console.log(checklistObj.date);
                checklistObj.save(function (err) {
                    if (err) return callback(err);
                    return callback();
                });
            }
        );
    };

    var importDayClosing = function (callback) {
        ClosingChecklistTemplate.find({'shift': 'day'}).exec(function (err, tempChecklists) {
            var date = new Date();
            date.setHours(8, 0, 0, 0);
            importTasks(tempChecklists, date, function (err) {
                if (err) return callback(err);
                return callback();
            });
        });
    };

    var importNightClosing = function (callback) {
        ClosingChecklistTemplate.find({'shift': 'night'}).exec(function (err, tempChecklists) {
            var date = new Date();
            date.setHours(17, 0, 0, 0);
            importTasks(tempChecklists, date, function (err) {
                if (err) return callback(err);
                return callback();
            });
        });
    };

    async.parallel([importDayClosing, importNightClosing], function (err) {
        if (err) return callback(err);
        return callback();
    });
};

exports.testTask = function (req, res) {
//TODO remember to take this off;

        exports.createFromTemplate(null, function (err) {
            if (err) {
                return res.status(400).send({
                    message: errorHandler.getErrorMessage(err)
                });
            } else {
                if(res) res.send('All OK');
            }
        });



};

/**
 * Create a Checklist
 */
exports.create = function (req, res) {
    var checklist = new Checklist(req.body);
    checklist.user = req.user;

    checklist.save(function (err) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.jsonp(checklist);
        }
    });
};

/**
 * Show the current Checklist
 */
exports.read = function (req, res) {
    res.jsonp(req.checklist);
};

/**
 * Update a Checklist
 */
exports.update = function (req, res) {
    console.log('Executing update function in Closing checklist');
    var checklist = req.checklist;
    checklist.user = req.user;
    checklist = _.extend(checklist, req.body);
    //debugger;
    console.dir(checklist);

        checklist.save(function (err) {
            if (err) {
                console.log('Er');
                console.log(err);
                return res.status(400).send({
                    message: errorHandler.getErrorMessage(err)
                });
            } else {
                if(checklist.isLocked){

                    mailjet.emailChecklistConfirmation('closing',req.user.displayName,checklist._id);
                }
                res.jsonp(checklist);
            }
        });



};


exports.updateTask =function(req,res){
    console.log('Update Task Called...');
    //console.log(req.checklist);
    var task = req.body;
    var checklist = req.checklist;

    Checklist.update({'checklists._id':task.taskId},
        {'$set':{
            'checklists.$.isCompleted':task.isCompleted,
            'checklists.$.comment':task.comment,
            'checklists.$.user':req.user._id
        }
        },function(err){
            if(err){
                console.log(err);
                return res.status(400).send(err);
            } else{
                console.log('Taskupdate Successful');
                res.jsonp(checklist);
            }

        });

};

/**
 * Delete an Checklist
 */
exports.delete = function (req, res) {
    var checklist = req.checklist;

    checklist.remove(function (err) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.jsonp(checklist);
        }
    });
};

/**
 * List of Checklists
 */
exports.list = function (req, res) {

    var date, shift;
    var oldDate = new Date(req.query.date);

    date = new Date();
    date.setDate(oldDate.getDate());
    date.setMonth(oldDate.getMonth());
    date.setFullYear(oldDate.getFullYear());

    if(req.query.shift === 'day'){
        date.setHours(8,0,0,0);
    } else if(req.query.shift === 'night'){
        date.setHours(17,0,0,0);
    }
    
    Checklist.find({'date':date}).sort('-created').populate('user', 'displayName').populate('checklists.user','displayName').exec(function (err, checklists) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.jsonp(checklists);
        }
    });
};

/**
 * Checklist middleware
 */
exports.checklistByID = function (req, res, next, id) {
    Checklist.findById(id).populate('user', 'displayName').exec(function (err, checklist) {
        if (err) return next(err);
        if (!checklist) return next(new Error('Failed to load Checklist ' + id));
        req.checklist = checklist;
        next();
    });
};

/**
 * Checklist authorization middleware
 */
exports.hasAuthorization = function (req, res, next) {
    if (req.checklist.user.id !== req.user.id) {
        return res.status(403).send('User is not authorized');
    }
    next();
};
