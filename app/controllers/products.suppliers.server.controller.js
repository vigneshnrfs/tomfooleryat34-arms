'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	errorHandler = require('./errors.server.controller'),
	Product = mongoose.model('Product.Supplier'),
	Supplier = mongoose.model('Supplier'),
	cronJob = require('./lib/costUpdates.server.lib'),
	_ = require('lodash');

/**
 * Create a Product
 */
exports.create = function(req, res) {
	var supplier = req.supplier;
	var product = new Product(req.body);
	product.supplier = supplier;

	product.save(function(err){
		if(err){
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			supplier.products.push(product);
			supplier.save(function(err){
				if(err){
					return res.status(400).send({
						message: errorHandler.getErrorMessage(err)
					});
				} else {
					res.jsonp(product);
				}
			});
		}
	});
};

/**
 * Show the current Product
 */
exports.read = function(req, res) {

	console.dir(req.ingredient);

	res.jsonp(req.ingredient);

};

/**
 * Update a Product
 */
exports.update = function(req, res) {

	var product = req.product ;

	product = _.extend(product , req.body);

	product.save(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(product);
			console.log('Updating all recipes associated with the ingredient.');
			cronJob.processIngredientCostChange(product._id,function(){
				console.log('Ingredient Cost Update Completed');
			});
		}
	});
};

/**
 * Delete a Product
 */
exports.delete = function(req, res) {
	var product = req.product ;
/*
	product.remove(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(product);
		}
	});
	*/
	res.send('ok');
};

/**
 * List of Products
 */
exports.list = function(req, res) { 
	Product.find().sort('name').populate('supplier','name').exec(function(err, products) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(products);
		}
	});
};

/**
 * Ingredient middleware
 */
exports.productByID = function(req, res, next, id) {
	Product.findById(id).populate('supplier', 'name').exec(function(err, product) {
		if (err) return next(err);
		if (! product) return next(new Error('Failed to load Supplier ' + id));
		req.product = product ;
		next();
	});
};

/**
 * Ingredient authorization middleware
 */
exports.hasAuthorization = function(req, res, next) {
	if (req.ingredient.user.id !== req.user.id) {
		return res.status(403).send('User is not authorized');
	}
	next();
};
