'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	errorHandler = require('./errors.server.controller'),
	Supplier = mongoose.model('Supplier'),
	Product = mongoose.model('Product.Supplier'),
	_ = require('lodash');

/**
 * Create a Supplier
 */
exports.create = function(req, res) {
	var supplier = new Supplier(req.body);
	supplier.user = req.user;

	supplier.save(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(supplier);
		}
	});
};

exports.products = {
	add : function(req,res){
		var supplier = req.supplier;
		var product = new Product(req.body);
		product.supplier = supplier;

		product.save(function(err){
			if(err){
				return res.status(400).send({
					message: errorHandler.getErrorMessage(err)
				});
			} else {
				supplier.products.push(product);
				supplier.save(function(err){
					if(err){
						return res.status(400).send({
							message: errorHandler.getErrorMessage(err)
						});
					} else {
						res.jsonp(product);
					}
				});
			}
		});
	},
	update: function(req, res) {
		var product = req.product ;

		product = _.extend(product , req.body);

		product.save(function(err) {
			if (err) {
				return res.status(400).send({
					message: errorHandler.getErrorMessage(err)
				});
			} else {
				res.jsonp(product);
			}
		});
	},

	delete: function(req, res){
		var product = req.product ;

		product.remove(function(err) {
			if (err) {
				return res.status(400).send({
					message: errorHandler.getErrorMessage(err)
				});
			} else {
				res.jsonp(product);
			}
		});
	}
};



/**
 * Show the current Supplier
 */
exports.read = function(req, res) {

	res.jsonp(req.supplier);

};

/**
 * Update a Supplier
 */
exports.update = function(req, res) {
	var supplier = req.supplier ;

	supplier = _.extend(supplier , req.body);

	supplier.save(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(supplier);
		}
	});
};

/**
 * Delete an Supplier
 */
exports.delete = function(req, res) {
	var supplier = req.supplier ;

	supplier.remove(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(supplier);
		}
	});
};

/**
 * List of Suppliers
 */
exports.list = function(req, res) { 
	Supplier.find().sort('-created').populate('user', 'displayName').populate('products').exec(function(err, suppliers) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(suppliers);
		}
	});
};




/**
 * Supplier middleware
 */
exports.supplierByID = function(req, res, next, id) { 
	Supplier.findById(id).populate('products').exec(function(err, supplier) {
		if (err) return next(err);
		if (! supplier) return next(new Error('Failed to load Supplier ' + id));

		req.supplier = supplier ;
		next();
	});
};

exports.productByID = function(req, res, next, id) {
	Product.findById(id).exec(function(err, product) {
		if (err) return next(err);
		if (! product) return next(new Error('Failed to load Supplier ' + id));
		req.product = product ;
		next();
	});
};

/**
 * Supplier authorization middleware
 */
exports.hasAuthorization = function(req, res, next) {
	if (req.supplier.user.id !== req.user.id) {
		return res.status(403).send('User is not authorized');
	}
	next();
};
