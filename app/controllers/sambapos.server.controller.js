'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    _ = require('lodash'),

    MenuItem = mongoose.model('MenuItem.pos'),
    GroupCode = mongoose.model('GroupCode.pos'),
    pos = require('./pos/sync/pos.sync.js');

exports.syncFromPos = function(req,res){
  pos.syncFromServer(function(err,result){
     if(err) return res.send(err);
      return res.jsonp(result);
  });
};
