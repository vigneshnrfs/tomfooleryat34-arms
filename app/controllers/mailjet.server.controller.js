'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    _ = require('lodash'),
    path = require('path'),
    Mailjet = require('mailjet-sendemail'),
    swig = require('swig'),
    emailTemplates = require('swig-email-templates'),
    mailjetConfig = require('../../config/config').mailjetSettings,
    nodemailer = require('nodemailer'),
    OpeningChecklists = mongoose.model('OpeningChecklist'),
    ClosingChecklists = mongoose.model('ClosingChecklist'),
    async = require('async');

var mailjet = new Mailjet(mailjetConfig.apiKey, mailjetConfig.secretKey);
var transporter = nodemailer.createTransport({
    service: 'Mailjet',
    auth: {
        user: mailjetConfig.apiKey,
        pass: mailjetConfig.secretKey
    }
});

var checklistRecepients;
if (process.env.NODE_ENV === 'production') {
    checklistRecepients = ['vignesh@tomfooleryat34.com', 'manzur@tomfooleryat34.com',
        'yanisa@tomfooleryat34.com', 'brice@tomfooleryat34.com',
        'anthony@tomfooleryat34.com'];

} else if (process.env.NODE_ENV === 'development') {
    checklistRecepients = ['vignesh@tomfooleryat34.com'];
}

exports.emailDailySales = function (date, data, callback) {
    var options = {
        root: './app/views/templates'
    };
    emailTemplates(options, function (err, render) {

        if (err) return callback(err);

        var salesItems = _.groupBy(data.salesDetail, 'GroupCode');

        var meta = data.floorCountAndTotals;
        //console.log(salesItems);
        meta.firstFloor.salesPerCover = meta.firstFloor.sales / meta.firstFloor.covers;
        meta.groundFloor.salesPerCover = meta.groundFloor.sales / meta.groundFloor.covers;
        meta.barTab.salesPerCover = meta.barTab.sales / meta.barTab.covers;

        meta.firstFloor.salesPerCover = meta.firstFloor.salesPerCover.toFixed(2);
        meta.groundFloor.salesPerCover = meta.groundFloor.salesPerCover.toFixed(2);
        meta.barTab.salesPerCover = meta.barTab.salesPerCover.toFixed(2);

        meta.total = {};
        meta.total.sales = meta.firstFloor.sales + meta.groundFloor.sales + meta.barTab.sales;
        meta.total.covers = meta.firstFloor.covers + meta.groundFloor.covers + meta.barTab.covers;
        meta.total.salesPerCover = (meta.total.sales / meta.total.covers).toFixed(2);


        var context = {
            date: date,
            salesItems: salesItems,
            meta: data.floorCountAndTotals
        };

        render('sales.email.view.html', context, function (err, html, text) {
            if (err) return callback(err);

            // setup e-mail data with unicode symbols
            var mailOptions = {
                from: 'Walter Jenkins<autobots@tomfooleryat34.com>', // sender address
                to: checklistRecepients, // list of receivers
                subject: 'Tomfoolery at 34 - Sales Report - ' + date.toISOString().substring(0, 10), // Subject line
                html: html // html body
            };

            // send mail with defined transport object
            transporter.sendMail(mailOptions, function (error, response) {
                if (error) {
                    console.log(error);
                    return callback(error);
                } else {
                    callback();
                }

                // if you don't want to use this transport object anymore, uncomment following line
                //smtpTransport.close(); // shut down the connection pool, no more messages
            });


        });
    });
};


exports.emailChecklistConfirmation = function (type, userName, checklistId) {


    if (type === 'opening') {

        OpeningChecklists.findById(checklistId).populate('checklists.user', 'displayName').exec(function (err, doc) {
            if (err) return console.log(err);

            renderAndEmail(doc);
        });
    } else if (type === 'closing') {

        ClosingChecklists.findById(checklistId).populate('checklists.user', 'displayName').exec(function (err, doc) {
            if (err) return console.log(err);

            renderAndEmail(doc);
        });
    }

    var renderAndEmail = function (checklist) {
        var shift;
        var url = 'http://localhost:3000/#!/checklists/' + type + '/' + shift + '?date=' + checklist.date + '';

        if (checklist.date.getHours() === 8) {
            shift = 'day';
        } else if (checklist.date.getHours() === 17) {
            shift = 'evening';
        } else {
            shift = 'You Failed. lol';
        }

        var checklists = _.groupBy(checklist.checklists, 'group');
        var content = swig.renderFile('app/views/templates/checklists.server.view.html', {
            type: type,
            shift: shift,
            date: checklist.date,
            userName: userName,
            url: url,
            comments: checklist.shiftComment,
            checklists: checklists
        });
        var subject = type + ' Sheet - ' + shift + ' Completed';

        async.each(checklistRecepients, function (to, callback) {
            mailjet.sendContent('vignesh@tomfooleryat34.com', to, subject, 'html', content);
            callback();
        }, function () {
            console.log('All Emails sent successfully.');
        });


    };


};

