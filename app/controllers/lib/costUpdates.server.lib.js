'use strict';
var mongoose = require('mongoose'),

    Recipe = mongoose.model('Recipe'),
    Supplier = mongoose.model('Supplier'),
    MenuItem = mongoose.model('MenuItem.pos'),
    Ingredient = mongoose.model('Product.Supplier'),
    pos = require('../pos/sync/pos.sync'),
    async = require('async'),
    _ = require('lodash');

var getIngredient = function (ingredientId, callback) {
    console.log('Getting the ingredient data for id : ', ingredientId);
    Ingredient.findById(ingredientId).exec(function (err, ingredient) {
        return callback(err, ingredient);
    });
};

var getRecipesByIngredientId = function (ingredientId, callback) {
    console.log('Getting all recipes for the ingredientId : ', ingredientId);
    Recipe.find({'ingredientItems.ingredient': ingredientId}).exec(function (err, recipes) {
        return callback(err, recipes);
    });
};

/**
 * Get the list of recipes from the baserecipes used.
 * @param {string} baseRecipeId - Id of the recipe that needs to be queried.
 * @param {function} callback - Callback Function.
 */
var getRecipesByBaseRecipeId = function (baseRecipeId, callback) {
    Recipe.find({'baseRecipeItems.recipe': baseRecipeId}).exec(function (err, recipes) {
        if (err)return callback(err);
        return callback(null, recipes);
    });
};

/**
 * Get the menuitems for which the recipe has changed.
 * @param {string} recipeId
 * @param {function} callback
 */
var getMenuItemsByRecipe = function (recipeId, callback) {
    MenuItem.find({'portions.activeRecipe': recipeId}).exec(function (err, menuItems) {
        if (err) return callback(err);
        return callback(null, menuItems);
    });
};

/***
 *
 * @param {object} ingredient - The ingredient data.
 * @param {object[]} recipes - Array of recipe models that needs to be updated.
 * @param {function} callback - Callback function.
 */
var updateRecipeCostForIngredient = function (ingredient, recipes, callback) {
    async.each(recipes, function (recipe, callback) {
            async.each(recipe.ingredientItems,
                function (item, callback) {
                    if (item.ingredient.equals(ingredient._id)) {
                        //Its a match. Update the cost and save the recipe.
                        item.unitCost = ingredient.unitPrice;
                        recipe.save(function (err) {
                            if (err) return callback(err);
                            return callback();

                        });
                    }
                },
                function callbackFn(err) {
                    if (err) return callback(err);
                    return callback();
                }
            );
        },
        //Callback Function
        function (err) {
            if (err) return callback(err);
            return callback(null, recipes);
        }
    );
};


/**
 * Update all the recipes for which the base recipes have changed.
 * @param {object[]} baseRecipes - The array of base recipes that have changed.
 * @param {function} callback - Callback function.
 */
var updateRecipeCostForBaseRecipes = function (baseRecipes, callback) {
    async.each(baseRecipes,
        function (baseRecipe, callback) {
            var processRecipeCosts = function (recipes, callback) {
                async.each(recipes,
                    function (recipe, callback) {
                        async.each(recipe.baseRecipeItems,
                            function (item, callback) {
                                if (item.recipe.equals(baseRecipe._id)) {
                                    item.unitCost = recipe.unitCost;
                                    item.totalCost = recipe.unitCost * item.quantity;
                                    recipe.save(function (err) {
                                        if (err) return callback(err);
                                        return callback();
                                    });
                                }

                            },
                            function (err) {
                                if (err)return callback(err);
                                return callback();
                            }
                        );

                    },
                    function (err) {
                        if (err) return callback(err);
                        return callback();
                    }
                );
            };

            getRecipesByBaseRecipeId(baseRecipe._id, function (err, recipes) {
                if (err) return callback(err);
                processRecipeCosts(recipes, function (err) {
                    if (err)return callback(err);
                    return callback(null, recipes);
                });
            });
        }, function (err, updatedRecipes) {
            //Callback function for above.
            if (err) return callback(err);
            return callback(null, updatedRecipes);
        }
    );
};

/**
 * Update the MenuItems for which the recipes has changed
 * @param {object[]} recipes - Recipes for which the data has changed.
 * @param {function} callback - callback function.
 */
var updateMenuItemCosts = function (recipes, callback) {

    if(!recipes) return callback();
    async.each(recipes,
        function (recipe, callback) {
            var processMenuItemCosts = function (menuItems, callback) {
                async.each(menuItems,
                    function (menuItem, callback) {
                        async.each(menuItem.portions,
                            function (portion, callback) {
                                if (portion.activeRecipe.equals(recipe._id)) {
                                    portion.cost = recipe.portionCost;
                                    menuItem.save(function (err) {
                                        if (err) return callback(err);
                                        return callback();
                                    });
                                }
                            },
                            function (err) {
                                if (err) return callback(err);
                                return callback();
                            }
                        );
                    },
                    function (err) {
                        if (err) return callback(err);
                        return callback();
                    }
                );
            };

            getMenuItemsByRecipe(recipe._id, function (err, menuItems) {
                if (err) {
                    return callback(err);
                } else {
                    processMenuItemCosts(menuItems, function (err) {
                        if (err)return callback(err);
                        return callback(null, menuItems);

                    });
                }
            });
        },
        function (err, updatedMenuItems) {
            if (err) return callback(err);
            return callback(null, updatedMenuItems);
        }
    );
};

/**
 * Update the Menu Item Costs on the POS
 * @param {object[]} menuItems - MenuItems that needs to be updated in the POS
 * @param {function} callback - Callback Function.
 */
var updateMenuItemCostsToPos = function (menuItems, callback) {
    async.each(menuItems, function (menuItem, callback) {
            pos.UpdateMenuItemOnServer(menuItem._id, function (err) {
                if (err)return callback(err);
                return callback();
            });
        }, function (err) {
            if (err) return callback(err);
            return callback();
        }
    );
};

var finalFunc = function (err) {

};


exports.processIngredientCostChange = function (ingredientId, callback) {
    //Get the Ingredient Model
    async.waterfall([
        //Get the ingredient details from the ingredient id.
        function (callback) {
            getIngredient(ingredientId, function (err, ingredient) {
                if (err)return callback(err);
                //console.log('Ingredient Details :', ingredient);
                return callback(null, ingredient);
            });
        },
        //Get all the recipes in which the ingredient is used.
        function (ingredient, callback) {
            getRecipesByIngredientId(ingredientId, function (err, recipes) {
                if (err) return callback(err);
                console.log('Recipes matching ingredient : ', recipes);
                return callback(null, ingredient, recipes);
            });
        },
        //Get all recipes updated.
        function (ingredient, recipes, callback) {
            console.log('---- Recipe Update Process ----');
            console.log('Ingredient : ', ingredient.name);
            console.log('Recipes Count : ', recipes.length);
            updateRecipeCostForIngredient(ingredient, recipes, function (err, recipes) {
                if (err) return callback(err);

                updateRecipeCostForBaseRecipes(recipes, function (err, updatedRecipes) {
                  if(!updatedRecipes) return callback(null,recipes);
                    async.forEach(updatedRecipes, function (recipe, callback) {
                        recipes.push(recipe);
                        callback();
                    }, function () {
                        return callback(null, recipes)
                    })
                });
            });
        },
        //Update All Menu items
        function (recipes, callback) {
            console.log('Recipes for which Menu Needs Updating : ', recipes);
            updateMenuItemCosts(recipes, function (err, menuItems) {
                if (err) return callback(err);
                return callback(null, menuItems);
            });
        }
    ], function (err, menuItems) {
        console.log('err ',err);
        if (err) return callback(err);
        return callback();
    });

};

