'use strict';

var schedule = require('node-schedule');
var OpeningChecklist = require('../controllers/checklists/opening.checklists.server.controller');
var ClosingChecklist = require('../controllers/checklists/closing.checklists.server.controller');
var reports = require('../controllers/pos/reports.server.controller.js');
var mailjet = require('../controllers/mailjet.server.controller.js');



var createChecklists = schedule.scheduleJob('30 2 * * *',function(){
    OpeningChecklist.testTask();
    ClosingChecklist.testTask();
});

//Email Daily Sales Reports
var emailDailySalesReports = schedule.scheduleJob('1 1 * * *',function(){
    console.log('Running schedule job for Daily Sales');
    var fromDate = new Date();
    fromDate.setDate(fromDate.getDate()-1);
    var toDate = new Date();

    reports.processSalesData(fromDate,toDate,function(err,results){
        if(err) return console.log(err);
        mailjet.emailDailySales(fromDate,results,function(err){
            if(err) return console.log(err);
            console.log('Daily Sales Email Sent Successfully');
        });
    });

});

