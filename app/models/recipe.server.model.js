'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    async = require('async'),
Schema = mongoose.Schema;

/**
 * Recipe Schema
 */
var RecipeSchema = new Schema({
    name: {
        type: String,
        default: '',
        required: 'Please fill Recipe name',
        trim: true
    },

    ingredientItems: [{
        ingredient: {type: Schema.ObjectId, ref: 'Product.Supplier'},
        quantity: {type: Number, required: true},
        unitCost: {type: Number, required: true},
        totalCost: {type: Number, required: true}
    }],

    baseRecipeItems: [{
        recipe: {type: Schema.ObjectId, ref: 'Recipe'},
        quantity: {type: Number, required: true},
        unitCost: {type: Number, required: true},
        totalCost: {type: Number, required: true}
    }],

    weight: {
        type: Number
    },

    yield: {
        type: Number,
        default: 0
    },

    totalWeight: {
        type: Number
    },
    //For the recipe. Not individual portion
    totalCost: {
        type: Number
    },

    portionSize: {
        type: Number
    },

    //per portion cost
    portionCost: {
        type: Number
    },

    //cost per unit (gms)
    unitCost: {
        type: Number
    },

    created: {
        type: Date,
        default: Date.now
    },

    user: {
        type: Schema.ObjectId,
        ref: 'User'
    }
});

RecipeSchema.pre('save', function (next) {

    var model = this;
    var weight = 0;
    var totalCost = 0;
    async.series([
            function (callback) {
                async.each(model.ingredientItems,
                    function(item,callback){
                        item.totalCost = item.quantity * item.unitCost;
                        weight += parseFloat(item.quantity);
                        totalCost += parseFloat(item.totalCost);
                        return callback();
                    },
                    function(){
                        return callback();
                    }
                );

            },
            function (callback) {

                async.each(model.baseRecipeItems,
                    function(item,callback){
                        item.totalCost = item.quantity * item.unitCost;
                        weight += parseFloat(item.quantity);
                        totalCost += parseFloat(item.totalCost);
                        return callback();
                    },
                    function(){
                        return callback();
                    }
                );

            }],
        function (err) {
            model.totalWeight = weight + model.yield;
            model.totalCost = totalCost;
            model.portionCost = model.totalCost / model.portionSize;
            model.unitCost = model.totalCost / model.totalWeight;
            next();
        }
    );


});

mongoose.model('Recipe', RecipeSchema);
