'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * Checklist Schema
 */
var ChecklistSchema = new Schema({
	group: {
		type: String,
		default: '',
		required: 'Please fill group name',
		trim: true
	},
	taskName: {
		type: String,
		default: '',
		required: 'Please fill task name',
		trim: true
	},
	shift:{
		type: String,
		default: 'Day',
		required: 'Please fill shift Day/Night',
		trim:'True'
	},
	created: {
		type: Date,
		default: Date.now
	},
	user: {
		type: Schema.ObjectId,
		ref: 'User'
	}
});

mongoose.model('ClosingChecklistTemplate', ChecklistSchema);
