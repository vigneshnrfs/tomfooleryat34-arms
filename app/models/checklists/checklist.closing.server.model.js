'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

/**
 * Checklist Schema
 */
var ClosingChecklistSchema = new Schema({
    date: {
        type: Date,
        default: Date.now
    },
    checklists: [{
        group: {
            type: String,
            trim: true
        },
        name: {
            type: String,
            default: '',
            required: 'Please fill Checklist name',
            trim: true
        },
        isCompleted: {
            type: Boolean,
            default: false
        },
        comment: {
            type: String,
            default: '',
            trim: true
        },
        user: {
            type: Schema.Types.ObjectId,
            ref: 'User'
        }
    }],
    shiftComment: {
        type: String,
        default: '',
        trim: true
    },

    created: {
        type: Date,
        default: Date.now
    },

    isLocked: {
        type: Boolean,
        default: false
    },

    user: {
        type: Schema.ObjectId,
        ref: 'User'
    }
});

mongoose.model('ClosingChecklist', ClosingChecklistSchema);
