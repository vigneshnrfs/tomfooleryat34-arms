'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

/**
 * GroupCode Schema
 */
var GroupCodeSchema = new Schema({
    name: {
        type: String,
        default: '',
        required: 'Please fill GroupCode name',
        trim: true
    },

    meta: {
        created: {
            type: Date,
            default: Date.now
        },

        lastUpdate:{
            type: Date
        }
    }

});

GroupCodeSchema.pre('save', function(next){
    this.lastUpdate = new Date();
    next();
});

mongoose.model('GroupCode.pos', GroupCodeSchema);
