'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema;


/**
 * MenuItem Schema
 */
var MenuItemSchema = new Schema({
    name: {
        type: String,

        required: 'Please fill MenuItem name',
        trim: true
    },

    groupCode: {
        type: String,
        required: 'Please fill Group Code name',
        trim: true

    },
    posId: {type: Number},
    portions: [{
        name: {
            type: String,
            required: true
        },
        activeRecipe: {
            type: Schema.ObjectId,
            ref: 'Recipe'
        },
        recipeItems: [{
            recipe: {
                type: Schema.ObjectId,
                ref: 'Recipe'
            }
        }],
        price: {
            type: Number,
            required: true
        },
        cost: {
            type: Number

        },
        priceExclVAT: {
            type: Number
        },
        grossProfit: {
            type: Number
        },
        meta: {
            posId: {
                type: Number
            }

        }
    }],

    meta: {
        created: {
            type: Date,
            default: Date.now
        },
        posId: {
            type: Number
        },
        lastUpdate: {
            type: Date
        }

    }

});

MenuItemSchema.pre('save', function (next) {

    this.lastUpdate = new Date();
    next();
});

mongoose.model('MenuItem.pos', MenuItemSchema);
