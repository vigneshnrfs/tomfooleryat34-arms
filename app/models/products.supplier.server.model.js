'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema;


/**
 * Ingredient Schema
 */
var IngredientSchema = new Schema({
    name: {
        type: String,
        default: '',
        required: 'Please fill Ingredient name',
        trim: true
    },

    unitPrice: {
        type: Number,
        required: true
    },

    units: {
        type: String,
        default: 'gms'
    },

    supplier:{
        type: Schema.ObjectId,
        ref: 'Supplier'
    },

    created: {
        type: Date,
        default: Date.now
    },
    user: {
        type: Schema.ObjectId,
        ref: 'User'
    }
});

mongoose.model('Product.Supplier', IngredientSchema);
