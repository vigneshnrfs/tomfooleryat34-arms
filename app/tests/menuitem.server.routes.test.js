'use strict';

var should = require('should'),
	request = require('supertest'),
	app = require('../../server'),
	mongoose = require('mongoose'),
	User = mongoose.model('User'),
	Menuitem = mongoose.model('MenuItem'),
	agent = request.agent(app);

/**
 * Globals
 */
var credentials, user, menuitem;

/**
 * MenuItem routes tests
 */
describe('MenuItem CRUD tests', function() {
	beforeEach(function(done) {
		// Create user credentials
		credentials = {
			username: 'username',
			password: 'password'
		};

		// Create a new user
		user = new User({
			firstName: 'Full',
			lastName: 'Name',
			displayName: 'Full Name',
			email: 'test@test.com',
			username: credentials.username,
			password: credentials.password,
			provider: 'local'
		});

		// Save a user to the test db and create new MenuItem
		user.save(function() {
			menuitem = {
				name: 'MenuItem Name'
			};

			done();
		});
	});

	it('should be able to save MenuItem instance if logged in', function(done) {
		agent.post('/auth/signin')
			.send(credentials)
			.expect(200)
			.end(function(signinErr, signinRes) {
				// Handle signin error
				if (signinErr) done(signinErr);

				// Get the userId
				var userId = user.id;

				// Save a new MenuItem
				agent.post('/menuitems')
					.send(menuitem)
					.expect(200)
					.end(function(menuitemSaveErr, menuitemSaveRes) {
						// Handle MenuItem save error
						if (menuitemSaveErr) done(menuitemSaveErr);

						// Get a list of Menuitems
						agent.get('/menuitems')
							.end(function(menuitemsGetErr, menuitemsGetRes) {
								// Handle MenuItem save error
								if (menuitemsGetErr) done(menuitemsGetErr);

								// Get Menuitems list
								var menuitems = menuitemsGetRes.body;

								// Set assertions
								(menuitems[0].user._id).should.equal(userId);
								(menuitems[0].name).should.match('MenuItem Name');

								// Call the assertion callback
								done();
							});
					});
			});
	});

	it('should not be able to save MenuItem instance if not logged in', function(done) {
		agent.post('/menuitems')
			.send(menuitem)
			.expect(401)
			.end(function(menuitemSaveErr, menuitemSaveRes) {
				// Call the assertion callback
				done(menuitemSaveErr);
			});
	});

	it('should not be able to save MenuItem instance if no name is provided', function(done) {
		// Invalidate name field
		menuitem.name = '';

		agent.post('/auth/signin')
			.send(credentials)
			.expect(200)
			.end(function(signinErr, signinRes) {
				// Handle signin error
				if (signinErr) done(signinErr);

				// Get the userId
				var userId = user.id;

				// Save a new MenuItem
				agent.post('/menuitems')
					.send(menuitem)
					.expect(400)
					.end(function(menuitemSaveErr, menuitemSaveRes) {
						// Set message assertion
						(menuitemSaveRes.body.message).should.match('Please fill MenuItem name');
						
						// Handle MenuItem save error
						done(menuitemSaveErr);
					});
			});
	});

	it('should be able to update MenuItem instance if signed in', function(done) {
		agent.post('/auth/signin')
			.send(credentials)
			.expect(200)
			.end(function(signinErr, signinRes) {
				// Handle signin error
				if (signinErr) done(signinErr);

				// Get the userId
				var userId = user.id;

				// Save a new MenuItem
				agent.post('/menuitems')
					.send(menuitem)
					.expect(200)
					.end(function(menuitemSaveErr, menuitemSaveRes) {
						// Handle MenuItem save error
						if (menuitemSaveErr) done(menuitemSaveErr);

						// Update MenuItem name
						menuitem.name = 'WHY YOU GOTTA BE SO MEAN?';

						// Update existing MenuItem
						agent.put('/menuitems/' + menuitemSaveRes.body._id)
							.send(menuitem)
							.expect(200)
							.end(function(menuitemUpdateErr, menuitemUpdateRes) {
								// Handle MenuItem update error
								if (menuitemUpdateErr) done(menuitemUpdateErr);

								// Set assertions
								(menuitemUpdateRes.body._id).should.equal(menuitemSaveRes.body._id);
								(menuitemUpdateRes.body.name).should.match('WHY YOU GOTTA BE SO MEAN?');

								// Call the assertion callback
								done();
							});
					});
			});
	});

	it('should be able to get a list of Menuitems if not signed in', function(done) {
		// Create new MenuItem model instance
		var menuitemObj = new Menuitem(menuitem);

		// Save the MenuItem
		menuitemObj.save(function() {
			// Request Menuitems
			request(app).get('/menuitems')
				.end(function(req, res) {
					// Set assertion
					res.body.should.be.an.Array.with.lengthOf(1);

					// Call the assertion callback
					done();
				});

		});
	});


	it('should be able to get a single MenuItem if not signed in', function(done) {
		// Create new MenuItem model instance
		var menuitemObj = new Menuitem(menuitem);

		// Save the MenuItem
		menuitemObj.save(function() {
			request(app).get('/menuitems/' + menuitemObj._id)
				.end(function(req, res) {
					// Set assertion
					res.body.should.be.an.Object.with.property('name', menuitem.name);

					// Call the assertion callback
					done();
				});
		});
	});

	it('should be able to delete MenuItem instance if signed in', function(done) {
		agent.post('/auth/signin')
			.send(credentials)
			.expect(200)
			.end(function(signinErr, signinRes) {
				// Handle signin error
				if (signinErr) done(signinErr);

				// Get the userId
				var userId = user.id;

				// Save a new MenuItem
				agent.post('/menuitems')
					.send(menuitem)
					.expect(200)
					.end(function(menuitemSaveErr, menuitemSaveRes) {
						// Handle MenuItem save error
						if (menuitemSaveErr) done(menuitemSaveErr);

						// Delete existing MenuItem
						agent.delete('/menuitems/' + menuitemSaveRes.body._id)
							.send(menuitem)
							.expect(200)
							.end(function(menuitemDeleteErr, menuitemDeleteRes) {
								// Handle MenuItem error error
								if (menuitemDeleteErr) done(menuitemDeleteErr);

								// Set assertions
								(menuitemDeleteRes.body._id).should.equal(menuitemSaveRes.body._id);

								// Call the assertion callback
								done();
							});
					});
			});
	});

	it('should not be able to delete MenuItem instance if not signed in', function(done) {
		// Set MenuItem user
		menuitem.user = user;

		// Create new MenuItem model instance
		var menuitemObj = new Menuitem(menuitem);

		// Save the MenuItem
		menuitemObj.save(function() {
			// Try deleting MenuItem
			request(app).delete('/menuitems/' + menuitemObj._id)
			.expect(401)
			.end(function(menuitemDeleteErr, menuitemDeleteRes) {
				// Set message assertion
				(menuitemDeleteRes.body.message).should.match('User is not logged in');

				// Handle MenuItem error error
				done(menuitemDeleteErr);
			});

		});
	});

	afterEach(function(done) {
		User.remove().exec();
		Menuitem.remove().exec();
		done();
	});
});
