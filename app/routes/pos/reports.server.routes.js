'use strict';

module.exports = function(app) {
	var users = require('../../controllers/users.server.controller.js');
	var reports = require('../../controllers/pos/reports.server.controller.js');

	// Reports Routes
	app.route('/reports/sales')
		.get(reports.sales);


	// Finish by binding the Report middleware
	//app.param('reportId', reports.reportByID);
};
