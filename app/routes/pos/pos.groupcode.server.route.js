'use strict';

module.exports = function(app) {
    var users = require('../../controllers/users.server.controller.js');

    var pos = require('../../controllers/pos.server.controller.js');
    var groupCode = pos.groupCode;

    // Menuitems Routes
    app.route('/pos/groupcode')
        .get(groupCode.list)
        .post(users.requiresLogin, groupCode.create);


    app.route('/pos/groupcode/:groupCodeId')
        .get(users.requiresLogin,  groupCode.read)
        .put(users.requiresLogin,  groupCode.update)
        .delete(users.requiresLogin, groupCode.delete);

    // Finish by binding the MenuItem middleware
    app.param('groupCodeId', groupCode.groupCodeByID);
};


