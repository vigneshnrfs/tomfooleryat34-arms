'use strict';

module.exports = function(app) {
	var users = require('../../controllers/users.server.controller.js');
	var menuItems = require('../../controllers/pos.server.controller.js');

	// Menuitems Routes
	app.route('/menuitems')
		.get(menuItems.list)
		.post(users.requiresLogin, menuItems.create);

	app.route('/menuitems/:menuItemId')
		.get(menuItems.read)
		.put(users.requiresLogin,  menuItems.update)
		.delete(users.requiresLogin, menuItems.delete);

	// Finish by binding the MenuItem middleware
	app.param('menuItemId', menuItems.menuItemByID);
};


