'use strict';


module.exports = function (app) {
    // Routing logic
    // ...
    var sambapos = require('../../app/controllers/sambapos.server.controller');
    var pos = require('../../app/controllers/pos/sync/pos.sync');

    // Recipes Routes
   /*
    app.route('/sambapos')
        .get(sambapos.connectTest);
    */
    app.route('/sambapos/syncfromserver')
        .get(sambapos.syncFromPos);



};
