'use strict';

module.exports = function(app) {
	var users = require('../../app/controllers/users.server.controller');
	var openingChecklist = require('../../app/controllers/checklists/opening.checklists.server.controller');
	var closingChecklist = require('../../app/controllers/checklists/closing.checklists.server.controller');
	var openingTemplate = require('../../app/controllers/checklists/opening.templates.checklists.server.controller');
	var closingTemplate = require('../../app/controllers/checklists/closing.templates.checklists.server.controller');

	// Checklists Routes

	app.route('/checklists/opening')
		.get(openingChecklist.list);

	app.route('/checklists/opening/import')
		.get(openingChecklist.testTask);

	app.route('/checklists/opening/:openingChecklistId')
		.get(openingChecklist.read)
		.put(users.requiresLogin,openingChecklist.update)
		.delete(users.requiresLogin,openingChecklist.delete)
		.patch(users.requiresLogin,openingChecklist.updateTask);

	app.route('/checklists/closing')
		.get(closingChecklist.list);

	app.route('/checklists/closing/import')
		.get(closingChecklist.testTask);
	app.route('/checklists/closing/:closingChecklistId')
		.get(closingChecklist.read)
		.put(users.requiresLogin,closingChecklist.update)
		.delete(users.requiresLogin,closingChecklist.delete)
		.patch(users.requiresLogin,closingChecklist.updateTask);




	app.route('/checklists/template/opening')
		.get(openingTemplate.list)
		.post(users.requiresLogin,openingTemplate.create);

	app.route('/checklists/template/copy')
		.get(openingTemplate.oneTime);

	app.route('/checklists/template/opening/:openingChecklistTaskId')
		.get(openingTemplate.read)
		.put(users.requiresLogin,openingTemplate.update)
		.delete(users.requiresLogin,openingTemplate.delete);

	app.route('/checklists/template/closing')
		.get(closingTemplate.list)
		.post(users.requiresLogin,closingTemplate.create);

	app.route('/checklists/template/closing/:closingChecklistTaskId')
		.get(closingTemplate.read)
		.put(users.requiresLogin,closingTemplate.update)
		.delete(users.requiresLogin,closingTemplate.delete);




	// Finish by binding the Checklist middleware
	app.param('openingChecklistId', openingChecklist.checklistByID);
	app.param('closingChecklistId', closingChecklist.checklistByID);
	app.param('openingChecklistTaskId', openingTemplate.checklistTaskByID);
	app.param('closingChecklistTaskId', closingTemplate.checklistTaskByID);
};
