'use strict';

module.exports = function(app) {
	var users = require('../../app/controllers/users.server.controller');
	var suppliers = require('../../app/controllers/suppliers.server.controller');
	var products = require('../../app/controllers/products.suppliers.server.controller');

	// Suppliers Routes
	app.route('/suppliers')
		.get(suppliers.list)
		.post(users.requiresLogin, suppliers.create);

	app.route('/suppliers/:supplierId')
		.get(suppliers.read)
		.put(users.requiresLogin, suppliers.update)
		.delete(users.requiresLogin, suppliers.delete);

	app.route('/suppliers/:supplierId/products')
		.post(users.requiresLogin,products.create);

	app.route('/suppliers/:supplierId/products/:productId')
		.put(users.requiresLogin, products.update)
		.delete(users.requiresLogin, products.delete);

	// Finish by binding the Supplier middleware
	app.param('supplierId', suppliers.supplierByID);

};
