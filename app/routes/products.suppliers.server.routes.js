'use strict';

module.exports = function(app) {
	var users = require('../../app/controllers/users.server.controller');
	var products = require('../controllers/products.suppliers.server.controller.js');

	// Ingredients Routes
	app.route('/products')
		.get(products.list)
		.post(users.requiresLogin, products.create);

	app.route('/products/:productId')
		.get(products.read)
		.put(users.requiresLogin, products.update)
		.delete(users.requiresLogin, products.delete);

	// Finish by binding the Ingredient middleware
	app.param('productId', products.productByID);

};
